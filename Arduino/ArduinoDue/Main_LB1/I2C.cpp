// 
// 
// 

#include "I2C.h"


//======================================================
// I2C helping functions
//======================================================

void i2c_write(int address, byte reg, byte data)
{
	// Send output register address
	Wire.beginTransmission(address);
	Wire.write(reg);
	// Connect to device and send byte
	Wire.write(data); // low byte
	Wire.endTransmission();
}

//================================================
//Used for reading the gyro??? 
//

void i2c_read(int address, byte reg, int quantity, byte* data)
{
	int i = 0;

	// Send input register address
	Wire.beginTransmission(address);
	Wire.write(reg);
	Wire.endTransmission();
	// Connect to device and request bytes
	int bytesReturned = Wire.requestFrom(address, quantity);

	//This seems to be an error indicator 
	if (bytesReturned != quantity)
	{
		digitalWrite(13, HIGH);
		Serial.print("E");
		Serial.write(1); //WTF is this
		//Serial.println("<I2C>Bytes returned:");
		//Serial.println(bytesReturned);
		return;
	}

	for (i = 0; i<quantity; i++) //Read buffer
	{
		char c = Wire.read(); // receive a byte as character
		data[i] = c;
	}
}