#ifndef HMC5983_h
#define HMC5983_h

//	WHAT IS THIS, THE GPS???

//==============================================================================================//
//                         HMC5983 Registers  //
//==============================================================================================// 

#define HMC5983_I2C_ADDR      0x1E

#define HMC5983REG_CONF_A     0x00
#define HMC5983REG_CONF_B     0x01
#define HMC5983REG_MODE       0x02
#define HMC5983REG_DATA_OUT_X_H   0x03
#define HMC5983REG_DATA_OUT_X_L   0x04
#define HMC5983REG_DATA_OUT_Z_H   0x05
#define HMC5983REG_DATA_OUT_Z_L   0x06
#define HMC5983REG_DATA_OUT_Y_H   0x07
#define HMC5983REG_DATA_OUT_Y_L   0x08

//==============================================================================================//
//                         HMC5983 Definitions  //
//==============================================================================================// 
#define BITS_CONF_A         0x9C
#define BITS_CONF_B         0x20
#define BITS_CONT_MODE        0x00 

#define mag2muTINT  24050//2^18





#endif




