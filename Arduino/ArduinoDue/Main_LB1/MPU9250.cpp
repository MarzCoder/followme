// 
// 
// 

#include "MPU9250.h"
#include "MPU9250_definitions.h"
#include "I2C.h"


//======================================================
// Initialization functions for MPU9250, HMC5983 and
//  u-blox M8N GPS
//======================================================
void init_MPU9250()
{
	i2c_write(MPU9250_I2C_ADDR, MPUREG_PWR_MGMT_1, BIT_H_RESET); //Reset board
	delay(100);

	init_acc();
	init_gyr();

}

void init_acc()
{
	i2c_write(MPU9250_I2C_ADDR, MPUREG_ACCEL_CONFIG, BITS_FS_8G); // Set +/- 8G as fullscale
	i2c_write(MPU9250_I2C_ADDR, MPUREG_ACCEL_CONFIG_2, BITS_DLPF_ACC_92HZ); //Set low pass to 92 Hz

}


void init_gyr()
{
	i2c_write(MPU9250_I2C_ADDR, MPUREG_GYRO_CONFIG, BITS_FS_2000DPS); //Set 2000 deg./sec as fullscale
	i2c_write(MPU9250_I2C_ADDR, MPUREG_CONFIG, BITS_DLPF_GYR_92HZ); //Set low pass to 92 Hz
}





//======================================================
// Read functions for Accelerometer, Gyroscope and Magnetometer
//======================================================

void readAccelerometer(short int* accData)
{
	byte temp[6];
	//Clear 6 spaces
	memset(accData, 0, 6);
	i2c_read(MPU9250_I2C_ADDR, MPUREG_ACCEL_XOUT_H_R, 6, temp);
	accData[0] = (short int)((temp[0] << 8) + temp[1]);
	accData[1] = (short int)((temp[2] << 8) + temp[3]);
	accData[2] = (short int)((temp[4] << 8) + temp[5]);
}

void readGyroscope(short int* gyrData)
{
	byte temp[6];
	//Clear 6 spaces
	memset(gyrData, 0, 6);
	i2c_read(MPU9250_I2C_ADDR, MPUREG_GYRO_XOUT_H_R, 6, temp);
	gyrData[0] = (short int)((temp[0] << 8) + temp[1]);
	gyrData[1] = (short int)((temp[2] << 8) + temp[3]);
	gyrData[2] = (short int)((temp[4] << 8) + temp[5]);
}



