// MPU9250.h

#ifndef _MPU9250_h
#define _MPU9250_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

//TODO: Do this as objectorientated code
void init_MPU9250();
void init_acc();
void init_gyr();
void init_mag(); 
void readGyroscope(short int* gyrData); 
void readMagnetometer(short int* magData);
void readAccelerometer(short int* accData);

extern short int* accData;
extern short int* gyrData;
extern short int* magData;


#endif

