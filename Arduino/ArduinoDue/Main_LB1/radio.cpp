// 
// 
// 

#include "radio.h"
#include <SPI.h>
#include "GPS.h"


//Forward declare the GPS and trackingsystem
extern uBloxGPS			GPS;
extern trackingSystem	tracking; 


static uint8_t addresses[][6] = { "1Node", "2Node" };
//static const char* role_friendly_name[] = { "invalid", "Ping out", "Pong back" };  // The debug-friendly names of those roles




struct dataStruct{
	unsigned long _micros;
	float value;
}myData;

void radio::setup()
{
	wireLess.begin();

	// Set the PA Level low to prevent power supply related issues since this is a
	// getting_started sketch, and the likelihood of close proximity of the devices. RF24_PA_MAX is default.
	wireLess.setPALevel(RF24_PA_LOW);

	// Open a writing and reading pipe on each radio, with opposite addresses
	if (radioNumber){
		wireLess.openWritingPipe(addresses[1]);
		wireLess.openReadingPipe(1, addresses[0]);
	}
	else{
		wireLess.openWritingPipe(addresses[0]);
		wireLess.openReadingPipe(1, addresses[1]);
	}

	myData.value = 1.22;
	// Start the wireLess listening for data
	wireLess.startListening();
	
}

char dummyCar[] = "Data from Rescue Runner";

char* radio::formatLastPacketAsASCII()
{

	return dummyCar;

}

//Copied from the RF24 example "Getting started handling data"
bool radio::loopBackTest()
{
	/****************** Ping Out Role ***************************/
	if (role == 1)  {

		wireLess.stopListening();                                    // First, stop listening so we can talk.


		Serial.println(F("Now sending"));

		myData._micros = micros();
		if (!wireLess.write(&myData, sizeof(myData))){
			Serial.println(F("failed"));
		}

		wireLess.startListening();                                    // Now, continue listening

		unsigned long started_waiting_at = micros();               // Set up a timeout period, get the current microseconds
		boolean timeout = false;                                   // Set up a variable to indicate if a response was received or not

		while (!wireLess.available()){                             // While nothing is received
			if (micros() - started_waiting_at > 200000){            // If waited longer than 200ms, indicate timeout and exit while loop
				timeout = true;
				break;
			}
		}

		if (timeout){                                             // Describe the results
			Serial.println(F("Failed, response timed out."));
		}
		else{
			// Grab the response, compare, and send to debugging spew
			wireLess.read(&myData, sizeof(myData));
			unsigned long time = micros();

			// Spew it
			Serial.print(F("Sent "));
			Serial.print(time);
			Serial.print(F(", Got response "));
			Serial.print(myData._micros);
			Serial.print(F(", Round-trip delay "));
			Serial.print(time - myData._micros);
			Serial.print(F(" microseconds Value "));
			Serial.println(myData.value);
		}

		// Try again 1s later
		delay(1000);
	}



	/****************** Pong Back Role ***************************/

	if (role == 0)
	{
		if (wireLess.available()){
			// Variable for the received timestamp
			while (wireLess.available()) {                          // While there is data ready
				wireLess.read(&myData, sizeof(myData));             // Get the payload
			}

			wireLess.stopListening();                               // First, stop listening so we can talk  
			myData.value += 0.01;                                // Increment the float value
			wireLess.write(&myData, sizeof(myData));              // Send the final one back.      
			wireLess.startListening();                              // Now, resume listening so we catch the next packets.     
			Serial.print(F("Sent response "));
			Serial.print(myData._micros);
			Serial.print(F(" : "));
			Serial.println(myData.value);
		}
	}




	/****************** Change Roles via Serial Commands ***************************/

	if (Serial.available())
	{
		char c = toupper(Serial.read());
		if (c == 'T' && role == 0){
			Serial.print(F("*** CHANGING TO TRANSMIT ROLE -- PRESS 'R' TO SWITCH BACK"));
			role = 1;                  // Become the primary transmitter (ping out)

		}
		else
			if (c == 'R' && role == 1){
				Serial.println(F("*** CHANGING TO RECEIVE ROLE -- PRESS 'T' TO SWITCH BACK"));
				role = 0;                // Become the primary receiver (pong back)
				wireLess.startListening();

			}
	}

	return false; //TODO: Return true if the loopback went fine
}

bool radio::poll()
{
	/****************** Ping Out Role ***************************/
	if (currentMainRole == MAIN_ROLE_PingOut)  
	{
#ifdef LEADER_BOAT
	
				/*==================================================
					WE SENT SOMETHING AND WAIT FOR THE RESCUE RUNNER TO SAY "OK"*/
				if (waitingForResponse)
				{
							if (wireLess.available())
							{
								readAllToBuffer(); 
								//TODO:	DECODE

								waitingForResponse = false; 
							}
							//TIMEOUT
							else if (millis() - started_waiting_at > timeOutResponse)
							{            
								HMIserial.println(F("Failed, response timed out."));
								waitingForResponse = false;
							}

				}
				else
				{/*===============================================================
						SEND STATE*/

						wireLess.stopListening();                                    // First, stop listening so we can talk.

						//SEND PRIORITY:  
						if (!tracking.wayPointsToSend.isEmpty())
						{
							Serial.println(F("Now sending waypoint "));
							sendData(DATA_NewWaypoint);
						}
						wireLess.startListening();                                    // Now, continue listening

						started_waiting_at = millis();       // Set up a timeout period, get the current microseconds
						timeout = false;                     // Set up a variable to indicate if a response was received or not
				}
	
#endif 
	}

	/****************** Pong Back Role ***************************/

	if (currentMainRole == MAIN_ROLE_PongBack)
	{
#ifdef RESCUE_RUNNER
		if (wireLess.available())
		{
			readAllToBuffer();

			/*=====================
			RESPOND ACCORDING TO WHAT WE RECEIVED*/

			wireLess.stopListening();                               // First, stop listening so we can talk  

			bool done = false; 

			while (buffer.available() && !done)
			{
				//All messages starts with a 1 byte ID 
				uint8_t messageID = buffer.read(); 

				switch (messageID)
				{
				/*=========================================================
				//See what data is requested and send it back*/
				case DATA_Request:
				{
									uint8_t typeOfDataRequested = buffer.read();

									switch (typeOfDataRequested)
									{
									case DATA_Orientation:

										sendData(DATA_Orientation);

										Serial.print(F("\tSent response "));
										Serial.print(myData._micros);
										Serial.print(F(" : "));
										Serial.println(thisVehicle.orientation.X);

										break;

									case DATA_Actuators:
										break;
									}
					done = true;
					break;
					}
				
				/*====================================
				We got information about the other vehicles orientation. 
				*/
				case DATA_Orientation:

					buffer.transferToStruct(&remoteVechicle.orientation, 
											sizeof(remoteVechicle.orientation)); 
				
					HMIserial.print("Got info about orientation, X: ");
					HMIserial.println(remoteVechicle.orientation.X); 

					//We reply by sending our own orientation back
					sendData(DATA_Orientation); 

					done = true; 
					break;


				/*===========================================================
				A new waypoint which we will try to navigate to.*/
				case DATA_NewWaypoint:
				{
					HMIserial.print(F("\tGot waypoint! "));
					

					trackingSystem::waypoint waypoint; 
					buffer.transferToStruct(&waypoint, sizeof(waypoint)); 

					tracking.wayPointsToClear.push(waypoint); 

					tracking.printWayPoint(waypoint); 

					//Is sent just to signal that we received smth. 
					sendData(DATA_Ready); 
					
					done = true; 
					break;
				}

				default: break; 
				}

				wireLess.startListening();	// Now, resume listening so we catch the next packets.    
			}
		}
#endif
	}

	return false; //TODO: What to return here

}

/* Sends data according to what is asked for*/
bool radio::sendData(dataType data)
{
	switch (data)
	{
	case DATA_Actuators:

		break;

	//Send the waypoint were currently pursing/the last waypoint that was issued
	case DATA_CurrentWaypoint:
	{
		trackingSystem::waypoint currentWaypoint; 
		currentWaypoint.ID = DATA_CurrentWaypoint;
#if defined LEADER_BOAT
		currentWaypoint = tracking.wayPointsToSend.back();
#elif defined RESCUE_RUNNER
		currentWaypoint = tracking.wayPointsToClear.front(); 
#endif
		return wireLess.write(&currentWaypoint, sizeof(currentWaypoint));
	
	}
	break; 


	case DATA_Orientation:
	{
		thisVehicle.orientation.ID = DATA_Orientation;

		thisVehicle.orientation.X += 0.1;  //DEBUG

		thisVehicle.orientation.Y = GPS.getY();

		return wireLess.write(&thisVehicle.orientation, sizeof(thisVehicle.orientation));
	}
	break; 

	////////////////////////////////////////////////
	///The leader boat issues new waypoints all the time
	case DATA_NewWaypoint:
	{
#ifdef LEADER_BOAT
		trackingSystem::waypoint waypoint = tracking.wayPointsToSend.pop();
		waypoint.ID = DATA_NewWaypoint; 
		return wireLess.write(&waypoint, sizeof(waypoint));
#endif

		break;
	}
	case DATA_Request:
			break; 

	case DATA_None:
	{
		//Is used to confirm reception of data etc. Dtich in favor or DATA_Ready? 
		uint8_t noData = DATA_None; 
		return wireLess.write(&noData, 1); 
		break;
	}

	case DATA_Parameters: break; 

	case DATA_Ready:  break;
	}

}


/*It seems that the RF24 only can read all data availabe at once. So we read it all, and decode it later*/
bool radio::readAllToBuffer()
{	
	if (wireLess.available() )
	{
		buffer.clear(); 
		wireLess.read(&buffer.data, maxPacketSize);
		return true; 
	}
	else
	{
		return false;
	} 
}

/*__________________SYSTEM SPECIFIC*/

#ifdef LEADER_BOAT
/*Only the leader boat is bus master and can request data!*/

bool radio::requestData(dataType data)
{
	uint8_t request[2] = { DATA_Request, uint8_t(data) };
	
	return wireLess.write(&request, 2); 
}

#elif defined RESCUE_RUNNER

/* The rescue runner pongs back to the leader boat saying its ready for new data*/
bool  radio::sendReady()
{
	static char ready = DATA_Ready; 

	return wireLess.write(&ready, 1); 
}

#endif





