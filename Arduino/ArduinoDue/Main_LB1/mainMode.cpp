// 
// 
// 

#include "mainMode.h"
#include "globals.h"


mainMode::mode mainMode::update(mode userRequest, mode theOtherVehiclesMode, uint32_t timeOfLastGPS, uint32_t lastUserRequest)
{

	/*================================================
	UI - If the UI didnt send for a long time, we think we lost contact with it. Then set to manual*/
	if (millis() - lastUserRequest > timeOuts::UI)
	{
		HMIserial.print(vehicleID);
		HMIserial.println(" had a timeout with the user interface connection. Setting main mode to 'Manual' ");

		currentMainMode = MAINMODE_Manual;

		return currentMainMode; 
	}


	/*======================================================
		If we had no GPS for a long time, usually set to manual*/
	if (millis() - timeOfLastGPS > timeOuts::GPS)
	{
		GPShasTimedOut = true;

		HMIserial.print(vehicleID); 
		HMIserial.println(" had a GPS timout. Setting main mode to 'Manual' "); 

		//When in handcontroller mode were fine without the GPS, but otherwise its safety critical
		if (currentMainMode != MAINMODE_Handcontroller)
		{
			currentMainMode = MAINMODE_Manual;
			return currentMainMode; 
		}
		
	}
	else
	{
		GPShasTimedOut = false; 
	}


	/*=========================================
		We passed the intial safety checks and go on and see if we should change the main mode*/

	//TODO:: 

	return currentMainMode; 
}