// I2C.h

#ifndef _I2C_h
#define _I2C_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include <Wire.h>


void i2c_read(int address, byte reg, int quantity, byte* data);
void i2c_write(int address, byte reg, byte data);



#endif

