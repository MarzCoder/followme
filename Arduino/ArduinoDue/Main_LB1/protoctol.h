
#ifndef __PROTOCOL_H
#define __PROTOCHOL_H


// These definitions are used to simplify the setup of the "dataToSend"
// variable.
#define SEND_Q				0x1
#define SEND_GYR			0x2
#define SEND_ACC			0x4
#define SEND_MAG			0x8
#define SEND_GPS			0x10
#define SEND_DELAY			0x20
#define SEND_P				0x40
#define SEND_SETTINGS		0x80
#define SEND_RR_CONTROL_OUT 0x100
#define SEND_POS			0x200
#define SEND_HEADING		0x400
#define SEND_RADIO_STATUS	0x800
#define SEND_MAGCORRECTION	0x1000

// Used to simplify the setup of the "settings" variable.
#define MAG_OFF 0x04

#endif

