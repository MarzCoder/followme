// 
// 
// 

#include "trackingSystem.h"


//Use the current GPS readings and see if there is a new waypoint


#ifdef LEADER_BOAT
void trackingSystem::generateNextWayPoint(float x, float y)
{
	if (distanceToLast(x, y) > minDist)
	{
		lastWaypoint.X = x;
		lastWaypoint.Y = y; 
		lastWaypoint.timeStamp = millis(); 
		lastWaypoint.number++; 

		wayPointsToSend.push( lastWaypoint	); 

		HMIserial.println("Generated new waypoint"); 
		printWayPoint(wayPointsToSend.back() );
	}
}
#elif defined RESCUE_RUNNER
/*If we are close enough to then next waypoint according to @clearance, but also long away enough from the leaderboat
@dist leader boat, we get the next waypoint*/
trackingSystem::waypoint getNextWaypoint(const float& x, const float& y, float clearance, float distToLeaderboat)
{
	//TODO: Update the current waypoint


}
#endif



/*Returns distance to a point and the latest waypoint*/
float trackingSystem::distanceToLast(float x, float y)
{
	return 	sqrt(  pow(x - lastWaypoint.X, 2) + pow(y - lastWaypoint.Y, 2	)	)	; 
}

/*Arguably the waypoint should be a class, but was affraid what would happen if the waypoint is sent as a class and not 
a struct over radio later*/
void trackingSystem::printWayPoint(waypoint T)
{
	HMIserial.print("Waypoint@ Timestamp: ");
	HMIserial.print(T.timeStamp); 
	HMIserial.print("\tNumber: ");
	HMIserial.print(T.number); 
	HMIserial.print("\t X: ");
	HMIserial.print(T.X);
	HMIserial.print("\t Y: ");
	HMIserial.print(T.Y);
	HMIserial.println(); 
}


