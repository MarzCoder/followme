// radio.h

#ifndef _RADIO_h
#define _RADIO_h

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif

#include "mainMode.h"
#include "globals.h"
#include "trackingSystem.h"


#include <RF24.h>

class radio 
{
public:

	radio(uint8_t CE, uint8_t CSN) : wireLess(CE, CSN){}; 

	bool poll();
	void setup(); 

	bool loopBackTest();

	/*______________RESULT STRUCTS - Here what we are going to send and what we received is saved
	 Any field can be added as long as the size doesnt exceed 32 bytes*/

	struct _orientation
	{
		uint8_t ID;
		float X, Y, hAcc, pitch, roll, jaw;
		uint16_t CRC;
	}; 

	struct _status
	{
		uint8_t ID; 
		mainMode::mode currentMainMode; 
		uint16_t flags; 
		uint16_t CRC;
	};

	struct _actuators
	{
		uint8_t ID; 
		float thrust, moment; 
		float alpha, beta; 
		uint16_t CRC;
	};

	struct PID
	{
		uint8_t		ID; 
		bool		newValue; 
		float		Kp, Ki, Kd, Tf; 

	}PIDa, PIDb, PIDc;

	//Summarizing struct, where all results are saved
	struct vehicle
	{
		_actuators		actuators; 
		_status			status; 
		_orientation	orientation; 

	}thisVehicle, remoteVechicle;

	/*Used to redirect the last data received to ASCII. Then we send this to matlab*/
	char* formatLastPacketAsASCII();

private:

	struct statistics
	{
		uint32_t fails, transactions; 
	}stats;

	enum mainRole
	{
		MAIN_ROLE_PongBack,
		MAIN_ROLE_PingOut,
		MAIN_ROLE_Disconnected,
	};

	enum radioID
	{
		RADIO_ID_LeaderBoat,
		RADIO_ID_RescueRunner,
	}; 

	// Role management: Set up role.  This sketch uses the same software for all the nodes
	// in this system.  Doing so greatly simplifies testing.  

	byte counter = 1;          



	RF24 wireLess; 


	bool waitingForMessage	= false; 
	bool lostConnection		= false; 

	/*Dont wait more than 2 seconds for a message*/
	const uint32_t timeOut = 2000; 

	/*======================================================
	  DEFINE WHAT ALL MESSAGES CONTAIN

	  A Frame looks as follows: 

	  [Synch char, synch char, messageType, length, [payload],crc1, crc2]
	  
	  */

	//Defines what messages type there are
	enum dataType
	{
		DATA_Orientation	 = 'O',		/*Sends GPS coordinates etc*/
		DATA_NewWaypoint	 = 'W',	    /*A new waypoint from the leaderboat*/
		DATA_CurrentWaypoint = 'C',	    /*Leaderboats current waypoint */
		DATA_Actuators		 = 'A',	    /*Information regarding actuators*/
		DATA_Parameters		 = 'P',		/*Request to change controller parameters*/
		DATA_Request		 = 'R',		/*Issued by the busmaster to request data*/
		DATA_Ready			 = 'D',		/*When rescue runner is ready for more data it issues this*/
		DATA_None			 = 'N',		/*USed for padding etc*/
	};

	static const uint8_t maxPacketSize = 32; //Acchording to the RF24 we can max write 32 bytes in each transaction
	static const uint8_t IDoffs		  = 1;   //ID offset 

	/* ___________BUFFER
	The idea with this is to get something similar to the arduino serial port class, as we know it well. 
	   The important difference is that the buffer has to be loaded from somwehere else first*/

	class _buffer
	{
		public:
		
		void clear()	  { i = 0; }; 
		int8_t available(){ return maxPacketSize - i; };
		
		/*Returns the next byte availabe and -1 if none is available (though a legit byte can be -1 too, check using availabel() first)*/
		int8_t read()
		{
			if (i < maxPacketSize){ 	return data[i++];  }
			else                  {     return -1;         }
		}

		uint8_t data[maxPacketSize];

		/*Copies the remaining size-1 bytes from the buffer to a struct. It doesnt 
		  copy the ID bytes, which are assumed to be read already. Returns true if succesful.
		  ATTENTION: Effecetive but dangerous function. If argument size is illdefind memory will be overwritten.*/
		bool transferToStruct(void *object, uint16_t size)
		{
			uint8_t* ptr = (uint8_t*)object;

			if (available() >= size - 1)
			{
				for (uint8_t k = IDoffs; k < size-IDoffs; k++)
				{
					ptr[k] = read();
				}

				return true; 
			}
			else
			{
				return false; 
			}


		}
		
		private: 
			
			uint8_t i; 

	}buffer;

	/*______________DEFINITIONS OF THE MESSAGES TYPE - All incoming messages will be CSV in the following order*/

	/*_____________Functions*/

	bool		readAllToBuffer();
	bool		loadOrientationStruct(); 
	bool		findNextMessagestart(); 
	uint8_t		readByte(); 

	void copyOverData(const uint8_t* copyFrom, void* copyTo, uint16_t len); 

	//Helpers
	bool sendData(dataType data);


//THINGS THAT ARE SPECIFIC FOR EACH VECHICLE
#ifdef LEADER_BOAT
	const uint32_t timeOutResponse = 400;			//Wait max 400 ms for a response
	unsigned long started_waiting_at;               // Set up a timeout period, get the current microseconds
	boolean timeout;
	bool waitingForResponse = false;
	static const uint8_t radioNumber = uint8_t(RADIO_ID_LeaderBoat);
	uint8_t role = 1; // The role of the current running sketch
	mainRole currentMainRole = MAIN_ROLE_PingOut;

	bool requestData(dataType data);

#elif defined RESCUE_RUNNER
	const uint8_t radioNumber = uint8_t(RADIO_ID_RescueRunner);
	uint8_t role = 0;                                // The role of the current running sketch
	mainRole currentMainRole = MAIN_ROLE_PongBack;

	bool sendReady();
	bool sendActuators(); 
#endif

};


#endif

