#include "QuaternionFunctionsInt.h"


// Variable initialization. Note that these are stored as integers. This is because int operations
// are much faster than float operations on the arduino due. To represent values smaller than one 
// and to achive the highest possible precision each variable are assigned an assumed scale.
// For example the array q used to hold the unit quaternion (quaternion with length 1). It is initialized
// to [1 0 0 0] but to avoid round of errors it is scaled up by 2^15 and becomes [32768 0 0 0] instead.
// A power of 2 is used since the shift operators << and >> can be used to change scale efficiently. 
// See Variable scales.ods for each assumed scale.
int32_t q[4] = { 32768, 0, 0, 0 }; // Array that holds the orientation estimate in quaternion format.
int32_t q0[4] = { 32768, 0, 0, 0 }; // Initial orientation guess.
// Covariance matrix belonging to the quaternion.
int64_t P[4][4] = { { 70368744, 0, 0, 0 }, { 0, 70368744, 0, 0 }, { 0, 0, 70368744, 0 }, { 0, 0, 0, 70368744 } };
//Initial values.
int64_t P0[4][4] = { { 70368744, 0, 0, 0 }, { 0, 70368744, 0, 0 }, { 0, 0, 70368744, 0 }, { 0, 0, 0, 70368744 } };


int32_t gyr[3] = { 0, 0, 0 }; // Array that holds the latest gyroscope measurements (xyz).
// Covariance matrix for the gyroscope measurements
int64_t Rw[3][3] = { { 175644275, -8996453, 5426 }, { -8996453, 137892623, -1445288 }, { 5426, -1445288, 108339063 } };

int32_t acc[3]; // Array that holds the latest accelerometer measurements (xyz).
// Covariance matrix for the accelerometer measurements
int64_t Ra[3][3] = { { 21805796051, 494807613, 431278077 }, { 494807613, 19206138668, 2577081565 }, { 431278077, 2577081565, 57967695683 } };
int32_t g0[3] = { 0, 0, 10056 }; // The nominal gravity vector
// Accelerometer bias
int32_t accBias[3] = { 115343, 188744, -193987 };

int32_t mag[3]; // Array that holds the latest magnetometer measurements (xyz).
// Covariance matrix for the magnetometer measurements
int64_t Rm[3][3] = { { 6871948, 0, 0 }, { 0, 6871948, 0 }, { 0, 0, 6871948 } };
int32_t m0[3] = { 256, 0, 0 }; // The nominal horizontal component of the Earth's magnetic field.
// Bias and scale error of the magnetometer
int32_t magBias[3] = { 158, -18, 72 }; //scale 2^0
int32_t magScale[3] = { 32072, 31105, 35431 }; //scale 2^15

// The step size of the filter 10486 corresponds to 0.0050001 seconds
int32_t stepSize = 10486;


void predictionStepAHRSINT32(int32_t q[4], int64_t P[4][4], int32_t gyro[3], int32_t stepSize, int64_t Rw[3][3]){
    int32_t S[4][4];
    int32_t Sq[4][3];
    int32_t newq[4];
    int64_t newP[4][4];
    int64_t P_plus_SP[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
//      int32_t SRS[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
    int64_t SR[4][3]={{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
    #ifdef debug
      Serial.println("---- Prediction Step ----");
      Serial.println("qin = ");
      for(int i=0;i<4;i++)
      {
        Serial.print(q[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    #endif
    #ifdef debug
        Serial.println("Pin = ");
        for(int i=0;i<4;i++)
        {
          for(int j=0;j<4;j++)
          {
            Serial.print((double)P[i][j]);
            Serial.print("\t");
          }
          Serial.println(" ");
          Serial.println(" ");
        }
    
    #endif
  
    #ifdef debug
        Serial.println("gyroin = ");
        for(int i=0;i<3;i++)
        {
          Serial.print(gyro[i]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
    #endif

    #ifdef debug
        Serial.println("stepSizein = ");
        Serial.println(stepSize);
        Serial.println(" ");
    #endif
    #ifdef debug
        Serial.println("Rwin = ");
        for(int i=0;i<3;i++)
        {
          for(int j=0;j<3;j++)
          {
            Serial.print((double)Rw[i][j]);
            Serial.print("\t");
          }
          Serial.println(" ");
          Serial.println(" ");
        }
    
    #endif
    SomegaINT32(S,gyro,stepSize);
    #ifdef debug
    Serial.println("S = ");
    for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
      {
        Serial.print(S[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }
    #endif
    X_plus_AxX_INT32(newq, q, S);
    q[0]=newq[0];
    q[1]=newq[1];
    q[2]=newq[2];
    q[3]=newq[3];
    #ifdef debug
    Serial.println("q = ");
    for(int i=0;i<4;i++)
    {
      Serial.print(q[i]);
      Serial.print("\t");
    }
    Serial.println(" ");
    Serial.println(" ");
    #endif

    calc_P_plus_SP_INT32(P_plus_SP, S, P); // B[4][4] + ((A[4][4]*B[4][4])>>15) 
    #ifdef debug
    Serial.println("P_plus_SP = ");
    for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
      {
        Serial.print((double)P_plus_SP[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }
    #endif
    calc_temp_P_INT32(newP, P_plus_SP, S); // A[4][4] + ((A[4][4]*B[4][4])>>15)
    #ifdef debug
    Serial.println("Ptemp = ");
    for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
      {
        Serial.print((double)newP[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }
    #endif
    SqfuncINT32(Sq, q, stepSize);
    #ifdef debug
    Serial.println("Sq = ");
    for(int i=0;i<4;i++)
    {
      for(int j=0;j<3;j++)
      {
        Serial.print(Sq[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }
    #endif

    calc_SRINT32(SR, Sq, Rw);
    #ifdef debug
    Serial.println("SR = ");
    for(int i=0;i<4;i++)
    {
      for(int j=0;j<3;j++)
      {
        Serial.print((double)SR[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }
    #endif
//    calc_SRS_INT32(SRS, SR, Sq);
//    #ifdef debug
//    Serial.println("SRS = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<4;j++)
//      {
//        Serial.print(SRS[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
    calc_P_plus_SRS_INT32(newP, SR, Sq);
    #ifdef debug
    Serial.println("newP = ");
    for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
      {
        Serial.print((double)newP[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }
    #endif
    for(int i=0;i<4;i++)
    {
      for(int j=0;j<4;j++)
      {
        P[i][j]=newP[i][j];
      }
    }
  
}

void updateStepAccAHRSINT32(int32_t q[4], int64_t P[4][4], int32_t acc[3], int64_t Ra[3][3], int32_t g0[3]){ //-------------------------------------

  int32_t Q[12][3];
  int32_t hd[3][4];// = {{0,0,0,0},{0,0,0,0},{0,0,0,0}};
  int32_t h[3];
  int64_t Phd[4][3];// = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
  int64_t S[3][3];// = {{0,0,0},{0,0,0},{0,0,0}};  
  //int32_t Sinv[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
  int32_t K[4][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
  int32_t diffAcch[3];
//  int64_t KS[4][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//        int32_t KSK[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};

  #ifdef debug
      Serial.println("---- Update Step Acc ----");
      Serial.println("qin = ");
      for(int i=0;i<4;i++)
      {
        Serial.print(q[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif
  #ifdef debug
      Serial.println("Pin = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<4;j++)
        {
          Serial.print((double)P[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif

  #ifdef debug
      Serial.println("accin = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(acc[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif

  #ifdef debug
      Serial.println("Rain = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print((double)Ra[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }  
  #endif
  #ifdef debug
      Serial.println("g0in = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(g0[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif
  dQqdqINT32(Q, q);
  #ifdef debug
    Serial.println("Q = ");
    for(int i=0;i<12;i++)
    {
      for(int j=0;j<3;j++)
      {
        Serial.print(Q[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }

  #endif
  calc_hdINT32(hd, Q, g0);
  #ifdef debug
      Serial.println("hd = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<4;j++)
        {
          Serial.print(hd[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  QqINT32(Q, q);
  #ifdef debug
      Serial.println("Q = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print(Q[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  calc_hINT32(h, Q, g0);
  #ifdef debug
      Serial.println("h = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(h[i]);
        Serial.print("\t");        
      }
      Serial.println(" ");
      Serial.println(" ");
  
  #endif
  calc_Phd_transposeINT32(Phd, P, hd);
  #ifdef debug
      Serial.println("Phd = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print((double)Phd[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  calc_SINT32(S, hd, Phd, Ra);
  #ifdef debug
      Serial.println("S = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print((double)S[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  calc_KINT32(K, Phd, S);
  #ifdef debug
      Serial.println("K = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print(K[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif

  calc_diffAcchINT32(diffAcch, acc, h);
  #ifdef debug
      Serial.println("diffAcch = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(diffAcch[i]);
        Serial.print("\t");
      }
      Serial.println(" ");  
      Serial.println(" ");
  #endif
  update_qINT32(q, K, diffAcch);
  #ifdef debug
      Serial.println("q = ");
      for(int i=0;i<4;i++)
      {
        Serial.print(q[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif

//  calc_KSK_INT32(KSK, Phd, K); 
//  #ifdef debug
//      Serial.println("KSK = ");
//      for(int i=0;i<4;i++)
//      {
//        for(int j=0;j<4;j++)
//        {
//          Serial.print(-KSK[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
  update_PINT32(P, Phd, K); 
  #ifdef debug
      Serial.println("P = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<4;j++)
        {
          Serial.print((double)P[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
}

void updateStepMagAHRSINT32(int32_t q[4], int64_t P[4][4], int32_t mag[3], int64_t Rm[3][3], int32_t m0[3]){ //-------------------------------------

  int32_t q1[4];
  int32_t Q[12][3];
  int32_t Q1[12][3];
  int32_t hd[3][4];// = {{0,0,0,0},{0,0,0,0},{0,0,0,0}};
  int32_t h[3];
  int64_t Phd[4][3];// = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
  int64_t S[3][3];// = {{0,0,0},{0,0,0},{0,0,0}};  
//  int32_t Sinv[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
  int32_t K[4][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
  int32_t mag1[3];
  int32_t diffAcch[3];
//  int64_t KS[4][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//        int32_t KSK[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};


//  q[0] = 30586;
//  q[1] = 2484;
//  q[2] = -475;
//  q[3] = 11488;
//
//  P[0][0] =262475; 
//  P[0][1] =-48554;
//  P[0][2] =-76702;
//  P[0][3] =-622060;
//  P[1][0] = -48554;
//  P[1][1] =408842;
//  P[1][2] =-4926;
//  P[1][3] =23222;
//  P[2][0] =-76702;
//  P[2][1] =-4926;
//  P[2][2] =405324;
//  P[2][3] =197032;
//  P[3][0] =-622060;
//  P[3][1] =23222;
//  P[3][2] =197032;
//  P[3][3] =1572741;
//
//  mag[0] = 849;
//  mag[1] = -2233;
//  mag[2] = -7870;

 

  
  #ifdef debug
      Serial.println("---- Update Step Mag----");
      Serial.println("qin = ");
      for(int i=0;i<4;i++)
      {
        Serial.print(q[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif
  #ifdef debug
      Serial.println("Pin = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<4;j++)
        {
          Serial.print((double)P[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif

  #ifdef debug
      Serial.println("magin = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(mag[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif

  #ifdef debug
      Serial.println("Rmin = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print((double)Rm[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }  
  #endif
  #ifdef debug
      Serial.println("m0in = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(m0[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif
  generateq1(q1,q);
//  q1[0]=q[0];
//  q1[1]=0;
//  q1[2]=0;
//  q1[3]=q[3];
//  normalizeQINT32(q1);
  dQqdqINT32(Q, q1);
  #ifdef debug
    Serial.println("Q = ");
    for(int i=0;i<12;i++)
    {
      for(int j=0;j<3;j++)
      {
        Serial.print(Q[i][j]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
    }

  #endif
  calc_hdINT32(hd, Q, m0);
  #ifdef debug
      Serial.println("hd = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<4;j++)
        {
          Serial.print(hd[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  QqINT32(Q, q1);
  #ifdef debug
      Serial.println("Q = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print(Q[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  calc_h_magINT32(h, Q, m0);
  #ifdef debug
      Serial.println("h = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(h[i]);
        Serial.print("\t");        
      }
      Serial.println(" ");
      Serial.println(" ");
  
  #endif
  calc_Phd_transposeINT32(Phd, P, hd);
  #ifdef debug
      Serial.println("Phd = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print((double)Phd[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  calc_S_magINT32(S, hd, Phd, Rm);
  #ifdef debug
      Serial.println("S = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print((double)S[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  calc_K_magINT32(K, Phd, S);
  #ifdef debug
      Serial.println("K = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print(K[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  calc_Q1_magINT32(Q1,q,q1);
  #ifdef debug
      Serial.println("Q1 = ");
      for(int i=0;i<3;i++)
      {
        for(int j=0;j<3;j++)
        {
          Serial.print(Q1[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
  rotate_magINT32(mag1,Q1,mag);
  #ifdef debug
        Serial.println("mag1 = ");
        for(int i=0;i<3;i++)
        {
          Serial.print(mag1[i]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
  #endif
  calc_diffAcchINT32(diffAcch, mag1, h);
  #ifdef debug
      Serial.println("diffAcch = ");
      for(int i=0;i<3;i++)
      {
        Serial.print(diffAcch[i]);
        Serial.print("\t");
      }
      Serial.println(" ");  
      Serial.println(" ");
  #endif
  update_q_magINT32(q, K, diffAcch);
  #ifdef debug
      Serial.println("q = ");
      for(int i=0;i<4;i++)
      {
        Serial.print(q[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif

//  calc_KSK_INT32(KSK, Phd, K); 
//  #ifdef debug
//      Serial.println("KSK = ");
//      for(int i=0;i<4;i++)
//      {
//        for(int j=0;j<4;j++)
//        {
//          Serial.print(-KSK[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
  update_P_magINT32(P, Phd, K); 
  #ifdef debug
      Serial.println("P = ");
      for(int i=0;i<4;i++)
      {
        for(int j=0;j<4;j++)
        {
          Serial.print((double)P[i][j]);
          Serial.print("\t");
        }
        Serial.println(" ");
        Serial.println(" ");
      }
  
  #endif
}

void generateq1(int32_t q1[4],int32_t q[4]){
  int32_t Q[12][3];
  float v1;
  float v2;
  float temp;
  QqINT32(Q, q);
  v1=Q[0][0]*0.000030517578125;
  v2=Q[1][0]*0.000030517578125;
  temp=atan2(v2,v1)/2;
  q1[0]=(int32_t)(cos(temp)*32768);
  q1[1]=0;
  q1[2]=0;
  q1[3]=(int32_t)(sin(temp)*32768);
  }

void rotate_magINT32(int32_t mag1[3],int32_t Q1[12][3],int32_t mag[3]){
  // A[3][3]*x[3] 
  mag1[0] = ((Q1[0][0]*mag[0])>>15) + ((Q1[0][1]*mag[1])>>15) + ((Q1[0][2]*mag[2])>>15);

  mag1[1] = ((Q1[1][0]*mag[0])>>15) + ((Q1[1][1]*mag[1])>>15) + ((Q1[1][2]*mag[2])>>15);
  
  mag1[2] = 0;
//  Serial.println("");
//  Serial.print("mag1: ");
//  Serial.println(mag1[0]);
//  Serial.println(mag1[1]);
//  Serial.println(" ");
    
  float temp = sqrt(sq(mag1[0])+sq(mag1[1]))/128;
  mag1[0] = mag1[0]/temp;
  mag1[1] = mag1[1]/temp; 
}

void calc_Q1_magINT32(int32_t Q1[12][3],int32_t q[4],int32_t q1[4]){
  int32_t qprim[4];
//  qprim[0] = (-q1[0]*q[0])>>15 - (q1[1]*q[1])>>15 - (q1[2]*q[2])>>15 - (q1[3]*q[3])>>15;
//  qprim[1] = (-q1[0]*q[1])>>15 + (q1[1]*q[0])>>15 + (q1[2]*q[3])>>15 - (q1[3]*q[2])>>15;
//  qprim[2] = (-q1[0]*q[2])>>15 - (q1[1]*q[3])>>15 + (q1[2]*q[0])>>15 + (q1[3]*q[1])>>15;
//  qprim[3] = (-q1[0]*q[3])>>15 + (q1[1]*q[2])>>15 - (q1[2]*q[1])>>15 + (q1[3]*q[0])>>15;
  qprim[0] = ((-q1[0]*q[0]) - (q1[1]*q[1]) - (q1[2]*q[2]) - (q1[3]*q[3]))>>15;
  qprim[1] = ((-q1[0]*q[1]) + (q1[1]*q[0]) + (q1[2]*q[3]) - (q1[3]*q[2]))>>15;
  qprim[2] = ((-q1[0]*q[2]) - (q1[1]*q[3]) + (q1[2]*q[0]) + (q1[3]*q[1]))>>15;
  qprim[3] = ((-q1[0]*q[3]) + (q1[1]*q[2]) - (q1[2]*q[1]) + (q1[3]*q[0]))>>15;
  #ifdef debug
      Serial.println("qprim = ");
      for(int i=0;i<4;i++)
      {
        Serial.print(qprim[i]);
        Serial.print("\t");
      }
      Serial.println(" ");
      Serial.println(" ");
  #endif
  QqINT32(Q1,qprim);
  
}

void QqINT32(int32_t Q[12][3], int32_t q[4]){
  int32_t q0=q[0];
  int32_t q1=q[1];
  int32_t q2=q[2];
  int32_t q3=q[3];
  
  int32_t qpows0=sq(q0);

  
  Q[0][0] = ((qpows0+sq(q1))>>14)-32768;
  Q[0][1] = (q1*q2-q0*q3)>>14;
  Q[0][2] = (q1*q3+q0*q2)>>14;
  Q[1][0] = (q1*q2+q0*q3)>>14;
  Q[1][1] = ((qpows0+sq(q2))>>14)-32768;
  Q[1][2] = (q2*q3-q0*q1)>>14;
  Q[2][0] = (q1*q3-q0*q2)>>14;
  Q[2][1] = (q2*q3+q0*q1)>>14;
  Q[2][2] = ((qpows0+sq(q3))>>14)-32768;
}


void dQqdqINT32(int32_t Q[12][3], int32_t q[4]){
  int32_t q0 = q[0];
  int32_t q1 = q[1];
  int32_t q2 = q[2];
  int32_t q3 = q[3];
  Q[0][0] = 4*q0;
  Q[0][1] = 2*-q3;
  Q[0][2] = 2*q2;
  Q[1][0] = 2*q3;
  Q[1][1] = 4*q0;
  Q[1][2] = 2*-q1;
  Q[2][0] = 2*-q2;
  Q[2][1] = 2*q1;
  Q[2][2] = 4*q0;
  Q[3][0] = 4*q1;
  Q[3][1] = 2*q2;
  Q[3][2] = 2*q3;
  Q[4][0] = 2*q2;
  Q[4][1] = 0;
  Q[4][2] = 2*-q0;
  Q[5][0] = 2*q3;
  Q[5][1] = 2*q0;
  Q[5][2] = 0;
  Q[6][0] = 0;
  Q[6][1] = 2*q1;
  Q[6][2] = 2*q0;
  Q[7][0] = 2*q1;
  Q[7][1] = 4*q2;
  Q[7][2] = 2*q3;
  Q[8][0] = 2*-q0;
  Q[8][1] = 2*q3;
  Q[8][2] = 0;
  Q[9][0] = 0;
  Q[9][1] = 2*-q0;
  Q[9][2] = 2*q1;
  Q[10][0] = 2*q0;
  Q[10][1] = 0;
  Q[10][2] = 2*q2;
  Q[11][0] = 2*q1;
  Q[11][1] = 2*q2;
  Q[11][2] = 4*q3;
  
  
}

void SqfuncINT32(int32_t Sq[4][3],int32_t q[4], int32_t T){
  Sq[0][0] = (-q[1]*T/2)>>15;
  Sq[0][1] = (-q[2]*T/2)>>15;
  Sq[0][2] = (-q[3]*T/2)>>15;
  Sq[1][0] = (q[0]*T/2)>>15;
  Sq[1][1] = (-q[3]*T/2)>>15;
  Sq[1][2] = (q[2]*T/2)>>15;
  Sq[2][0] = (q[3]*T/2)>>15;
  Sq[2][1] = (q[0]*T/2)>>15;
  Sq[2][2] = (-q[1]*T/2)>>15;
  Sq[3][0] = (-q[2]*T/2)>>15;
  Sq[3][1] = (q[1]*T/2)>>15;
  Sq[3][2] = (q[0]*T/2)>>15;
//  Sq[0][0] = (-q[1]*T/2)>>16;
//  Sq[0][1] = (-q[2]*T/2)>>16;
//  Sq[0][2] = (-q[3]*T/2)>>16;
//  Sq[1][0] = (q[0]*T/2)>>16;
//  Sq[1][1] = (-q[3]*T/2)>>16;
//  Sq[1][2] = (q[2]*T/2)>>16;
//  Sq[2][0] = (q[3]*T/2)>>16;
//  Sq[2][1] = (q[0]*T/2)>>16;
//  Sq[2][2] = (-q[1]*T/2)>>16;
//  Sq[3][0] = (-q[2]*T/2)>>16;
//  Sq[3][1] = (q[1]*T/2)>>16;
//  Sq[3][2] = (q[0]*T/2)>>16;
}

void SomegaINT32(int32_t S[4][4], int32_t gyro[3], int32_t T){ //^15
    S[0][0] = 0;
  S[0][1] = (-gyro[0]*T/2)>>14;
  S[0][2] = (-gyro[1]*T/2)>>14;
  S[0][3] = (-gyro[2]*T/2)>>14;
  S[1][0] = (gyro[0]*T/2)>>14;
  S[1][1] = 0;
  S[1][2] = (gyro[2]*T/2)>>14;
  S[1][3] = (-gyro[1]*T/2)>>14;
  S[2][0] = (gyro[1]*T/2)>>14;
  S[2][1] = (-gyro[2]*T/2)>>14;
  S[2][2] = 0;
  S[2][3] = (gyro[0]*T/2)>>14;
  S[3][0] = (gyro[2]*T/2)>>14;
  S[3][1] = (gyro[1]*T/2)>>14;
  S[3][2] = (-gyro[0]*T/2)>>14;
  S[3][3] = 0;
//  S[0][0] = 0;
//  S[0][1] = (-gyro[0]*T/2)>>21;
//  S[0][2] = (-gyro[1]*T/2)>>21;
//  S[0][3] = (-gyro[2]*T/2)>>21;
//  S[1][0] = (gyro[0]*T/2)>>21;
//  S[1][1] = 0;
//  S[1][2] = (gyro[2]*T/2)>>21;
//  S[1][3] = (-gyro[1]*T/2)>>21;
//  S[2][0] = (gyro[1]*T/2)>>21;
//  S[2][1] = (-gyro[2]*T/2)>>21;
//  S[2][2] = 0;
//  S[2][3] = (gyro[0]*T/2)>>21;
//  S[3][0] = (gyro[2]*T/2)>>21;
//  S[3][1] = (gyro[1]*T/2)>>21;
//  S[3][2] = (-gyro[0]*T/2)>>21;
//  S[3][3] = 0;
//    S[0][0] = 0;
//  S[0][1] = (-gyro[0]*T/2)>>16;
//  S[0][2] = (-gyro[1]*T/2)>>16;
//  S[0][3] = (-gyro[2]*T/2)>>16;
//  S[1][0] = (gyro[0]*T/2)>>16;
//  S[1][1] = 0;
//  S[1][2] = (gyro[2]*T/2)>>16;
//  S[1][3] = (-gyro[1]*T/2)>>16;
//  S[2][0] = (gyro[1]*T/2)>>16;
//  S[2][1] = (-gyro[2]*T/2)>>16;
//  S[2][2] = 0;
//  S[2][3] = (gyro[0]*T/2)>>16;
//  S[3][0] = (gyro[2]*T/2)>>16;
//  S[3][1] = (gyro[1]*T/2)>>16;
//  S[3][2] = (-gyro[0]*T/2)>>16;
//  S[3][3] = 0;
}

void SomegaINT64(int64_t S[4][4], int64_t gyro[3], int64_t T){
  S[0][0] = 0;
  S[0][1] = (-gyro[0]*T/2)>>32;
  S[0][2] = (-gyro[1]*T/2)>>32;
  S[0][3] = (-gyro[2]*T/2)>>32;
  S[1][0] = (gyro[0]*T/2)>>32;
  S[1][1] = 0;
  S[1][2] = (gyro[2]*T/2)>>32;
  S[1][3] = (-gyro[1]*T/2)>>32;
  S[2][0] = (gyro[1]*T/2)>>32;
  S[2][1] = (-gyro[2]*T/2)>>32;
  S[2][2] = 0;
  S[2][3] = (gyro[0]*T/2)>>32;
  S[3][0] = (gyro[2]*T/2)>>32;
  S[3][1] = (gyro[1]*T/2)>>32;
  S[3][2] = (-gyro[0]*T/2)>>32;
  S[3][3] = 0;
}


void normalizeQINT32(int32_t q[4]){
  int32_t norm = ((int64_t)sqrt(sq(q[0]>>5)+sq(q[1]>>5)+sq(q[2]>>5)+sq(q[3]>>5)));
//  Serial.print("q^2 = ");
//  Serial.print(sq(q[0]>>8)+sq(q[1]>>8)+sq(q[2]>>8)+sq(q[3]>>8));
//  Serial.print(" norm = ");
//  Serial.println(norm);
//    int32_t norm = SquareRoot((int64_t)sq(q[0])+(int64_t)sq(q[1])+(int64_t)sq(q[2])+(int64_t)sq(q[3]));
  if(q[0]<0)
    norm = -norm;
  for(int i=0;i<4;i++)
  {
    q[i]=(((int64_t)(q[i]))<<10)/norm;
  }
  
  
}

void limitP(int64_t* P,int64_t* P0)
{
  if(*P<500||*(P+5)<50000||*(P+10)<50000||*(P+15)<500)
  {
    resetP(P,P0);
//    for(int i=0;i<16;i++)
//    {
//      *P++=*P0++;
//    }
  }
}

void resetP(int64_t* P, int64_t* P0)
{
  for(int i=0;i<16;i++)
    {
      *P++=*P0++;
    }
}

void resetq(int32_t* q, int32_t* q0)
{
  for(int i=0;i<4;i++)
    {
      *q++=*q0++;
    }
}

float quaternion2Yaw(int32_t* q){
  float qt[4];
  qt[0] = (float)q[0]/32768;
  qt[1] = (float)q[1]/32768;
  qt[2] = (float)q[2]/32768;
  qt[3] = (float)q[3]/32768;
  float result = atan2(2.0*(qt[2]*qt[3] + qt[0]*qt[1]), qt[0]*qt[0] - qt[1]*qt[1] - qt[2]*qt[2] + qt[3]*qt[3])/3.1416*180;
//  float result = 2*acos((float)q[0]/32768/(sqrt(sq((float)q[0]/32768)+sq((float)q[2]/32768))));
  return result;
}

//uint32_t SquareRoot(uint32_t a_nInput){
//    uint32_t op  = a_nInput;
//    uint32_t res = 0;
//    uint32_t one = 1uL << 30; // The second-to-top bit is set: use 1u << 14 for uint16_t type; use 1uL<<30 for uint32_t type
//
//
//    // "one" starts at the highest power of four <= than the argument.
//    while (one > op)
//    {
//        one >>= 2;
//    }
//
//    while (one != 0)
//    {
//        if (op >= res + one)
//        {
//            op = op - (res + one);
//            res = res +  2 * one;
//        }
//        res >>= 1;
//        one >>= 2;
//    }
//    return res;
//}

//
//void predictionStepAHRSINT32(int32_t q[4], int32_t P[4][4], int32_t gyro[3], int32_t stepSize, int32_t Rw[3][3]){
//    int32_t S[4][4];
//    int32_t Sq[4][3];
//    int32_t newq[4];
//    int32_t newP[4][4];
//    int32_t P_plus_SP[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
//      int32_t SRS[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
//    int32_t SR[4][3]={{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//    #ifdef debug
//      Serial.println("---- Prediction Step ----");
//      Serial.println("qin = ");
//      for(int i=0;i<4;i++)
//      {
//        Serial.print(q[i]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    #endif
//    #ifdef debug
//        Serial.println("Pin = ");
//        for(int i=0;i<4;i++)
//        {
//          for(int j=0;j<4;j++)
//          {
//            Serial.print(P[i][j]);
//            Serial.print("\t");
//          }
//          Serial.println(" ");
//          Serial.println(" ");
//        }
//    
//    #endif
//  
//    #ifdef debug
//        Serial.println("gyroin = ");
//        for(int i=0;i<3;i++)
//        {
//          Serial.print(gyro[i]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//    #endif
//
//    #ifdef debug
//        Serial.println("stepSizein = ");
//        Serial.println(stepSize);
//        Serial.println(" ");
//    #endif
//    #ifdef debug
//        Serial.println("Rwin = ");
//        for(int i=0;i<3;i++)
//        {
//          for(int j=0;j<3;j++)
//          {
//            Serial.print(Rw[i][j]);
//            Serial.print("\t");
//          }
//          Serial.println(" ");
//          Serial.println(" ");
//        }
//    
//    #endif
//    SomegaINT32(S,gyro,stepSize);
//    #ifdef debug
//    Serial.println("S = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<4;j++)
//      {
//        Serial.print(S[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
//    X_plus_AxX_INT32(newq, q, S);
//    q[0]=newq[0];
//    q[1]=newq[1];
//    q[2]=newq[2];
//    q[3]=newq[3];
//    #ifdef debug
//    Serial.println("q = ");
//    for(int i=0;i<4;i++)
//    {
//      Serial.print(q[i]);
//      Serial.print("\t");
//    }
//    Serial.println(" ");
//    Serial.println(" ");
//    #endif
//
//    B_plus_AxB_INT32(P_plus_SP, S, P); // B[4][4] + ((A[4][4]*B[4][4])>>15) 
//    #ifdef debug
//    Serial.println("P_plus_SP = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<4;j++)
//      {
//        Serial.print(P_plus_SP[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
//    A_plus_AxB_transposeINT32(newP, P_plus_SP, S); // A[4][4] + ((A[4][4]*B[4][4])>>15)
//    #ifdef debug
//    Serial.println("Ptemp = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<4;j++)
//      {
//        Serial.print(newP[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
//    SqfuncINT32(Sq, q, stepSize);
//    #ifdef debug
//    Serial.println("Sq = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<3;j++)
//      {
//        Serial.print(Sq[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
//
//    calc_SRINT32(SR, Sq, Rw);
//    #ifdef debug
//    Serial.println("SR = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<3;j++)
//      {
//        Serial.print(SR[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
//    calc_SRS_INT32(SRS, SR, Sq);
//    #ifdef debug
//    Serial.println("SRS = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<4;j++)
//      {
//        Serial.print(SRS[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
//    calc_P_plus_SRS_INT32(newP, SR, Sq);
//    #ifdef debug
//    Serial.println("newP = ");
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<4;j++)
//      {
//        Serial.print(newP[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//    #endif
//    for(int i=0;i<4;i++)
//    {
//      for(int j=0;j<4;j++)
//      {
//        P[i][j]=newP[i][j];
//      }
//    }
//  
//}

//
//void updateStepAccAHRSINT32(int32_t q[4], int32_t P[4][4], int32_t acc[3], int32_t Ra[3][3], int32_t g0[3]){ //-------------------------------------
//
//  int32_t Q[12][3];
//  int32_t hd[3][4];// = {{0,0,0,0},{0,0,0,0},{0,0,0,0}};
//  int32_t h[3];
//  int32_t Phd[4][3];// = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//  int32_t S[3][3];// = {{0,0,0},{0,0,0},{0,0,0}};  
//  int32_t Sinv[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
//  int32_t K[4][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//  int32_t diffAcch[3];
//  int32_t KS[4][3] = {{0,0,0},{0,0,0},{0,0,0},{0,0,0}};
//        int32_t KSK[4][4]={{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
//
//  #ifdef debug
//      Serial.println("---- Update Step ----");
//      Serial.println("qin = ");
//      for(int i=0;i<4;i++)
//      {
//        Serial.print(q[i]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//  #endif
//  #ifdef debug
//      Serial.println("Pin = ");
//      for(int i=0;i<4;i++)
//      {
//        for(int j=0;j<4;j++)
//        {
//          Serial.print(P[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//
//  #ifdef debug
//      Serial.println("accin = ");
//      for(int i=0;i<3;i++)
//      {
//        Serial.print(acc[i]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//  #endif
//
//  #ifdef debug
//      Serial.println("Rain = ");
//      for(int i=0;i<3;i++)
//      {
//        for(int j=0;j<3;j++)
//        {
//          Serial.print(Ra[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }  
//  #endif
//  #ifdef debug
//      Serial.println("g0in = ");
//      for(int i=0;i<3;i++)
//      {
//        Serial.print(g0[i]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//  #endif
//  dQqdqINT32(Q, q);
//  #ifdef debug
//    Serial.println("Q = ");
//    for(int i=0;i<12;i++)
//    {
//      for(int j=0;j<3;j++)
//      {
//        Serial.print(Q[i][j]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//    }
//
//  #endif
//  calc_hdINT32(hd, Q, g0);
//  #ifdef debug
//      Serial.println("hd = ");
//      for(int i=0;i<3;i++)
//      {
//        for(int j=0;j<4;j++)
//        {
//          Serial.print(hd[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//  QqINT32(Q, q);
//  #ifdef debug
//      Serial.println("Q = ");
//      for(int i=0;i<3;i++)
//      {
//        for(int j=0;j<3;j++)
//        {
//          Serial.print(Q[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//  calc_hINT32(h, Q, g0);
//  #ifdef debug
//      Serial.println("h = ");
//      for(int i=0;i<3;i++)
//      {
//        Serial.print(h[i]);
//        Serial.print("\t");        
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//  
//  #endif
//  calc_Phd_transposeINT32(Phd, P, hd);
//  #ifdef debug
//      Serial.println("Phd = ");
//      for(int i=0;i<4;i++)
//      {
//        for(int j=0;j<3;j++)
//        {
//          Serial.print(Phd[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//  calc_SINT32(S, hd, Phd, Ra);
//  #ifdef debug
//      Serial.println("S = ");
//      for(int i=0;i<3;i++)
//      {
//        for(int j=0;j<3;j++)
//        {
//          Serial.print(S[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//  calc_KINT32(K, Phd, S);
//  #ifdef debug
//      Serial.println("K = ");
//      for(int i=0;i<4;i++)
//      {
//        for(int j=0;j<3;j++)
//        {
//          Serial.print(K[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//
//  calc_diffAcchINT32(diffAcch, acc, h);
//  #ifdef debug
//      Serial.println("diffAcch = ");
//      for(int i=0;i<3;i++)
//      {
//        Serial.print(diffAcch[i]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");  
//      Serial.println(" ");
//  #endif
//  update_qINT32(q, K, diffAcch);
//  #ifdef debug
//      Serial.println("q = ");
//      for(int i=0;i<4;i++)
//      {
//        Serial.print(q[i]);
//        Serial.print("\t");
//      }
//      Serial.println(" ");
//      Serial.println(" ");
//  #endif
//
//  calc_KSK_INT32(KSK, Phd, K); 
//  #ifdef debug
//      Serial.println("KSK = ");
//      for(int i=0;i<4;i++)
//      {
//        for(int j=0;j<4;j++)
//        {
//          Serial.print(-KSK[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//  update_PINT32(P, Phd, K); 
//  #ifdef debug
//      Serial.println("P = ");
//      for(int i=0;i<4;i++)
//      {
//        for(int j=0;j<4;j++)
//        {
//          Serial.print(P[i][j]);
//          Serial.print("\t");
//        }
//        Serial.println(" ");
//        Serial.println(" ");
//      }
//  
//  #endif
//}


