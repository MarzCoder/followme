// GPS.h

#ifndef _GPS_h
#define _GPS_h

/*
	Function reading the u-blox GPS. 

	Attention! This curren code has outlier detectors etc configured to work around gothenburg!!



*/

#if defined(ARDUINO) && ARDUINO >= 100
	#include "arduino.h"
#else
	#include "WProgram.h"
#endif
//======================================================
// GPS UBX NAV-PVT message format and handling
//======================================================
// The GPS receiver outputs its data over a serial bus.
// Only the NAV-PVT message is activated,
// see u-blox8-M8_ReceiverDescrProtSpec_(UBX-13003221)_Public.pdf
//
// Search for nav-pvt in the above manual. The GPS needs to be set to output a NAV-PVT message
// for it to work

//======================================================
// Initialization function for u-blox M8N GPS
//======================================================

class uBloxGPS
{

	public: 

		uBloxGPS(HardwareSerial* __GPSserial, uint32_t _baud, float _lat0 = 57.7089);
		void init(); 

		//Polls the GPS serial port and returns true when new valid data has been found
		bool poll(); 
		void readMagnetometer(short int* magData);

		float getLongitude();
		float getLatitude(); 

		/*Returns the GPS own accuracy estimate*/
		float getHorisontalAccuracy(); 

		float getX();
		float getY();
		
		/*Returns the last time valid data was received*/
		uint32_t getTimeOfLastData(); 

	private: 

		/*_____________Settings*/

		const uint32_t timeoutUs_perRead = 100;		//Wait maximally this long to get at least one character, ditch this?
		const uint32_t timeoutUs		= 1500000;	//Wait maximally this long for a whole GPS message

		/*______Conversion constants*/
		const  float		   r = 6371000; //Earth radius 
		const  float		lat0; //The latitude of where the gps usually operates (used for coordinate conversion)
	
		//float degToRad(float degrees); //Do as a macro instead

		/*_____________Outlier detection - anything outside this is an outlier*/
	
		struct outlier 
		{ 
			//The gps outputs an accuracy estimate in mm. If this is higher than this, we have an outliers
			static constexpr uint32_t accuracyEstimate = 100000; //So this is 100 meters 

			/*
			struct lat
			{
				static constexpr float max = lat0 + 5;
				static constexpr float min = lat0 - 5;
			};
			struct lon
			{
				static constexpr float max = 11 + 5;
				static constexpr float min = 11 - 5;
			};
			*/
			
			//Allow max one degree increase between each sample
			static constexpr float maxInc = 1; 
		};

	
		//Defines the length of each part of the message
		enum messageLengths
		{
			LENGTH_SYNCH	= 2, 
			LENGTH_ID		= 1,
			LENGTH_CLASS	= 1,
			LENGTH_LENGTH	= 2,
			LENGTH_PAYLOAD	= 92,
			LENGTH_CHKSUM	= 2
		};

		//The GPS messages is 96 bytes long + the two start bytes that are always the same
		constexpr static uint8_t GPSmessageLength = LENGTH_ID+
												    LENGTH_CLASS+
													LENGTH_LENGTH+
													LENGTH_PAYLOAD+
													LENGTH_CHKSUM;
		
		/*______________Private vars*/
		uint8_t messageBuffer[GPSmessageLength];		// Message from the GPS is always stored here
		
		/*The first for bytes are metadata. Offsets in the datasheet are defined after this metadata
		we use this to get indexing more similar to that in the datasheet*/

		static constexpr uint8_t  IDoffset = LENGTH_ID + LENGTH_CLASS + LENGTH_LENGTH;			

		/*Meta data */
		//TODO: Analyze the meta data sent to verify it	

		/* A number of pointers are defined for easier access of the data.*/
		uint32_t*	iTOW		= (uint32_t*)	(messageBuffer + IDoffset + 0);			//GPS time of the week
		uint8_t*	fixType		= (uint8_t*)	(messageBuffer + IDoffset + 20);		//Indicates if the GPS tried to "fix" a value
		uint8_t*	flags		= (uint8_t*)	(messageBuffer + IDoffset + 21);		//	Warnings etc 
		int32_t*	lon			= (int32_t*)	(messageBuffer + IDoffset + 24);		//  Longitude, is degrees * 10^7
		int32_t*	lat			= (int32_t*)	(messageBuffer + IDoffset + 28);		//  Latitude 
		uint32_t*	hAcc		= (uint32_t*)	(messageBuffer + IDoffset + 40);		//	Horistontal Accuracy estimate
		uint32_t*	vAcc		= (uint32_t*)	(messageBuffer + IDoffset + 44);		//	Vertical accuracy estimate 
		int32_t*	velN		= (int32_t*)	(messageBuffer + IDoffset + 48);		//	North velocity (mm/s)
		int32_t*	velE		= (int32_t*)	(messageBuffer + IDoffset + 52);		//	East velocity (mm/s)
		uint32_t*	gSpeed		= (uint32_t*)	(messageBuffer + IDoffset + 60);		//	Groundspeed (mm/s)
		int32_t*	headMot		= (int32_t*)	(messageBuffer + IDoffset + 64);		//	Heading of motion


		uint8_t*	CK_A		= (uint8_t*)	(messageBuffer + GPSmessageLength - 2 );				//	Checksums 
		uint8_t*	CK_B		= (uint8_t*)	(messageBuffer + GPSmessageLength - 1 );


		#define get_CK_A(x) (		x[ GPSmessageLength-2	 ]	)
		#define get_CK_B(x) (		x[ GPSmessageLength-1    ]	)

		bool foundGPSMsg = false; //Indicates that the start of a new GPS message

		/*Where we currently are in reading a GPS message*/
		uint8_t messageIndex = 0; 

		// has been found. Reset to 0 when the end is reached.
		uint32_t GPSReadStartTime = 0; // Time at which the start of the message
		// was read. If the rest of the message does not arrive within a certain 
		// time limit the message is assumed to be lost and the code start to look
		// for the next message.
		uint8_t newGPS = 0; // Set to 1 when a new complete message has arrived.
		uint32_t GPSFixTimer = 0; // Stores the time of the last NAV-PVT message
		// with a high fix ok flag.
		uint32_t lastReceivedGPS = 0; // Time of last received message.

		struct results
		{
			float velE;		/*, North, east velocity m/s*/
			float velN;
			float lon;		/*Degrees*/
			float lat;
			float speed;   /*m/s*/
			float x;	   /*meters*/
			float y; 
			uint32_t	   timeStamp;	/*millis()*/

		}currentResult;

		bool newValidGPSdata = false; 
		//Serial port
		HardwareSerial* GPSserial;
		const uint32_t baud;							//The gps baud setting

		//The GPS sometimes tries to fix it values, described in the messages "fixType"

		enum FIX_TYPE
		{
			FIX_TYPE_NO_FIX =0
		};
	
		#define FLAG_gnssFixOk	( *flags & (1<<0))
		#define FIX_noFix		(*fixType ==FIX_TYPE_NO_FIX)


		/*______________Private functions*/
		void  initMagnetometer(); 

		/*The NAV-PVT format outputs results that arent in SI units*/
		bool convertAndSaveResults(); 

		/*Sets everything NAN if we get an outlier */
		void setAllResultsNAN(); 

		/* Is current based on that GPS coord should fall within a specific interval. Thus this GPS library
		will only work in gothenburg!*/
		bool checkIfThereAreAnyOutliers(); 
		
		/* Calculates checksum according to the UBX standard*/
		bool calculateCheckSum(); 


};












#endif

