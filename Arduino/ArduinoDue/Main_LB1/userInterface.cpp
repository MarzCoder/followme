// 
// 
// 

#include "userInterface.h"
#include "globals.h"

////======================================================
//// Messages used to send raw sensor data to MATLAB
////======================================================
uint8_t accMessage[] = { 0x61, 0x62, 0x63, 0x64, 0x65, 0x66, 0x68, '\r', '\n' };
short int* accData = (short int*)(accMessage + 1);

uint8_t gyrMessage[] = { 0x67, 0x62, 0x63, 0x64, 0x65, 0x66, 0x68, '\r', '\n' };
short int* gyrData = (short int*)(gyrMessage + 1);

uint8_t magMessage[] = { 0x6D, 0x62, 0x63, 0x64, 0x65, 0x66, 0x68, '\r', '\n' };
short int* magData = (short int*)(magMessage + 1);



UI::UI()
{
	mode = 0;
	steeringAngleRef = 0;
	throttleRef = 0;
	messagePosition = 0;
	messageID = 0;
	lastByte = 0;

	newPIDD = 0;
	newPIDA = 0;
	newPIDV = 0;
	timeOfLastModeMessage = 0;
	_HardSerial = &Serial;
	_HardSerial->begin(115200);
}

UI::UI(HardwareSerial* hwSerial)
{
	mode = 0;
	steeringAngleRef = 0;
	throttleRef = 0;
	messagePosition = 0;
	messageID = 0;
	lastByte = 0;
	newPIDD = 0;
	newPIDA = 0;
	newPIDV = 0;
	timeOfLastModeMessage = 0;
	_HardSerial = hwSerial;
}


uint32_t UI::timeOfLastUImessage()
{
	return 0;  //TODO: Connect with some decent variable describing this 
}

mainMode::mode UI::getUserRequestedMode()
{
	//TODO: Integrate with rest of communication 
	return mainMode::MAINMODE_Manual; 
}


/////////////////////////////////////////////
/// Check for a new message bytes for x microseconds
int UI::checkMessages(uint16_t msgTimeOut)
{
	uint32_t timer = micros();

	byte lastByte;

	int messagesReceived = 0;

	///TODO: Change to while timer && available ?? 
	while (micros() - timer<msgTimeOut)
	{
		if (_HardSerial->available())
		{

			lastByte = _HardSerial->read();

			switch (messagePosition)
			{
			case 0:
				if (lastByte == 0x4D)  //0x4D=M
					messagePosition = 1;		//First part of message start detected.
				break;
			case 1:
				if (lastByte == 0x53)  //0x53=S
				{
					messagePosition = 2;        //Second part of message start detected.
				}
				else
				{
					HMIserial.print(vehicleID);
					Serial.println(" got invalid message!");
					messagePosition = 0;		//Error, go back
				}
				break;
			case 2:
				messageID = lastByte;
				messagePosition = 3;
				break;
			default:
				/*////////////////////////////////////////////////////
				� ///WE GOT A VALID PACKET AND WILL NOW DECODE IT*/
				switch (messageType(messageID))
				{
				case MESSAGETYPE_Mode:                 //Mode message
					mode = (uint8_t)lastByte;
					timeOfLastModeMessage = millis();
					messagePosition = 0;
					messagesReceived++;
					break;

				case MESSAGETYPE_Steering:                 //Steering message
					if (mode != 0)
					{
						steeringAngleRef = (int8_t)lastByte;
					}
					messagePosition = 0;
					messagesReceived++;
					break;

				case MESSAGETYPE_Throttle:                 //Throttle message
					if (mode != 0)
					{
						throttleRef = (uint8_t)lastByte;
					}
					messagePosition = 0;
					messagesReceived++;
					break;

				case MESSAGETYPE_PID_D_update :   //PID update - Three exactly equal PID parameters handlers. 
					//TODO: Add class that does this
				{
					PID_parametersD[0] = lastByte;

					int i = 1;

					while (i<16 && (micros() - timer<2000))
					{
						if (_HardSerial->available())
						{
							PID_parametersD[i] = _HardSerial->read();
							i++;
						}
					}

					//Check if successful read
					if (i == 16)
					{
						newPIDD = 1;
					}
					messagePosition = 0;
					messagesReceived++;
				}
				break;

				case MESSAGETYPE_PID_A_update:                 //PID update
				{
					PID_parametersA[0] = lastByte;
					int i = 1;

					while (i<16 && (micros() - timer<2000))
					{
						if (_HardSerial->available())
						{
							PID_parametersA[i] = _HardSerial->read();
							i++;
						}
					}

					if (i == 16)
					{
						newPIDA = 1;
					}

					messagePosition = 0;
					messagesReceived++;

				}
				break;

				case MESSAGETYPE_PID_V_update:                 
				{
					PID_parametersV[0] = lastByte;

					int i = 1;

					while (i<16 && (micros() - timer<2000))
					{
						if (_HardSerial->available())
						{
							PID_parametersV[i] = _HardSerial->read();
							i++;
						}
					}

					if (i == 16)
					{
						newPIDV = 1;
					}

					messagePosition = 0;
					messagesReceived++;
				}
				break;

				default:
					messagePosition = 0;  //ID not found
					break;
				}
				break;
			}
		}

		////////////////////////////////////
		///TIME OUT - Safety feature
		if (millis() - timeOfLastModeMessage>500)    //If the mode has not been updated for 500 ms
		{                                         //it is reset to zero. Safefty function???
			mode = 0;
		}

		if (mode == 0)
		{
			steeringAngleRef = 0;
			throttleRef = 0;
		}
	}

	return messagesReceived;
}
uint8_t UI::getMode()
{        //Checks if there has been a recent mode message and then returns the mode

	if (millis() - timeOfLastModeMessage>500)
	{
		mode = 0;
		steeringAngleRef = 0;
		throttleRef = 0;
	}

	return mode;
}
int8_t  UI::getSteeringAngleRef()
{
	if (millis() - timeOfLastModeMessage>500)
	{
		mode = 0;
		steeringAngleRef = 0;
		throttleRef = 0;
	}
	return steeringAngleRef;
}
uint8_t UI::getThrottleRef()
{
	if (millis() - timeOfLastModeMessage>500)
	{
		mode = 0;
		steeringAngleRef = 0;
		throttleRef = 0;
	}
	return throttleRef;
}
void  UI::sendSteeringAngle(int8_t angle)
{
	_HardSerial->print("MSA");      //Message start + 'A' for angle
	_HardSerial->write(angle);
}

uint8_t   UI::getNewPIDD()
{
	if (newPIDD == 1)
	{
		newPIDD = 0;
		return 1;
	}
	else
		return 0;
}
uint8_t   UI::getNewPIDA()
{
	if (newPIDA == 1)
	{
		newPIDA = 0;
		return 1;
	}
	else
		return 0;
}

uint8_t   UI::getNewPIDV()
{
	if (newPIDV == 1)
	{
		newPIDV = 0;
		return 1;
	}
	else
		return 0;
}

byte*     UI::getPIDD()
{
	return PID_parametersD;
}

byte*     UI::getPIDA()
{
	return PID_parametersA;
}
byte*     UI::getPIDV()
{
	return PID_parametersV;
}










