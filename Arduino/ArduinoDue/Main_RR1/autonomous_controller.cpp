#include "Guidance_support_functions.h"
#include "angle_PIDRR.h"
#include "autonomous_controller.h"
#include "Distance_PID.h"
#include "Velocity_PID.h"
#include "nrf24l01_RR.h"
static float rudder;
static float throttle;
static float y;
static float x;

void updateControl(int32_t posLB[2], float velLB[2],float headingQuaternionLB[2],float yawRateLB,
int32_t posRR[2], float velRR[2],float headingQuaternionRR[2],float yawRateRR,float lat2m, float lon2m){
  float posDiff[2];
  float velDiff[2];
  float posDiff_RRFrame[2];
  float errorDistance;
  float errorVelocity;
  float speedLB;
  float velRef;
  float errorAngle;
 

  
  float predPos[4];
  
  
  float rotatedx[2];
  float normSpeedLB;
  float normSpeedRR;
  float normDistance;
  float angleTarget;
  float angular_vel_refRR;

//  Serial.println("posLB");
//  Serial.println(posLB[0]);
//  Serial.println(posLB[1]);
//  Serial.println("");
//  
//  Serial.println("velLB");
//  Serial.println(velLB[0],5);
//  Serial.println(velLB[1],5);
//  Serial.println("");
  float temp[2];
  temp[0]= 1;
  temp[1]= 0;
  float tempRot[2];

if(getMode()==1) //Basic control
{
//    rotate2DByQuaternion(tempRot,headingQuaternionLB[0],headingQuaternionLB[1],temp);
//  Serial.println("heading LB");
//  Serial.println(atan2(tempRot[1],tempRot[0]),5);
//  Serial.println("");
//
//  Serial.println("yaw rate LB");
//  Serial.println(yawRateLB,5);
//  Serial.println("");
//
//  Serial.println("posRR");
//  Serial.println(posRR[0]);
//  Serial.println(posRR[1]);
//  Serial.println("");
//  
//  Serial.println("velRR");
//  Serial.println(velRR[0],5);
//  Serial.println(velRR[1],5);
//  Serial.println("");
  
  rotate2DByQuaternion(tempRot,headingQuaternionRR[0],headingQuaternionRR[1],temp);
//  Serial.println("heading RR");
//  Serial.println(atan2(tempRot[1],tempRot[0]),5);
//  Serial.println("");
  posDiff[0] = (posLB[0]-posRR[0])*lat2m/100;
  posDiff[1] = (posLB[1]-posRR[1])*lon2m/100;

  rotate2DByQuaternion(posDiff_RRFrame,headingQuaternionRR[0],headingQuaternionRR[1],posDiff);

  //errorDistance = norm2D(posDiff_RRFrame)-30;
  //errorDistance = norm2D(posDiff_RRFrame)-7;
  errorDistance = norm2D(posDiff_RRFrame)-getDistance();

  if(errorDistance>20)
  {
    errorDistance=20;
  }

  speedLB = norm2D(velLB);

  velRef = speedLB + updateOutput_Distance(errorDistance);

  errorVelocity = velRef-norm2D(velRR);

  throttle = updateOutput_Velocity(errorVelocity);
  if(throttle<0)
  {
    throttle=0;
  }
  else if(throttle>1)
  {
    throttle=1;
  }

  errorAngle = atan2(posDiff_RRFrame[1],posDiff_RRFrame[0]);

  rudder = updateOutput_angle(errorAngle);
  if(rudder<-1)
  {
    rudder=-1;
  }
  else if(rudder>1)
  {
    rudder=1;
  }
  
}
else if(getMode()==3) //Not used
{
  throttle=0;
  rudder=0;
}

}
void applyControl(){
//  sendSteeringRef((int8_t)(rudder*190.99));
//  sendThrottleRef((uint8_t)(throttle*100));
//if(y>100/190.99)
//{
//  y=100/190.99;
//}
//else if(y<-100/190.99)
//{
//  y=-100/190.99;
//}
//  sendSteeringRef((int8_t)(y*190.99));
//  if(x<0)
//    x=0;
//  sendThrottleRef((uint8_t)(x*100));

  sendSteeringRef((int8_t)(-rudder*100));
  Serial.println("rudder: ");
  Serial.println((int8_t)(rudder*100));
  
  sendThrottleRef((uint8_t)(throttle*100));
  Serial.println("throttle: ");
  Serial.println((uint8_t)(throttle*100));
  
}

void sendMode(int8_t mode){
  char modeMessage[4] = {'M','S',1,0};
  modeMessage[3] = mode;
  Serial2.write(modeMessage,4);
}

void sendSteeringRef(int8_t steeringRef){
  char steeringMessage[4] = {'M','S',2,0};
  steeringMessage[3] = steeringRef;
  Serial2.write(steeringMessage,4);
}
void sendThrottleRef(uint8_t throttleRef){
  char throttleMessage[4] = {'M','S',3,0};
  throttleMessage[3] = throttleRef;
  Serial2.write(throttleMessage,4);
}

int8_t getRudder(){
  return (int8_t)(rudder*100);
//   return (int8_t)(y*190.99);
}
uint8_t getThrottle(){
  return (uint8_t)(throttle*100);
//  return (uint8_t)(x*100);
}

