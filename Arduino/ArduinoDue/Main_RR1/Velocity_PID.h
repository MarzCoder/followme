void resetPID_Velocity();

void calcPIDParameters_Velocity();

float updateOutput_Velocity(float newInput);

float getLastOutput_Velocity();

void setPIDF_Velocity(float* parameters);
