#include <stdint.h>
#include <stdio.h>
#include "MPU9250_definitions.h"
#include "HMC5983_definitions.h"
#include <math.h>
#include "MatrixOperations.h"

//#define debug

//#ifndef Qdfg
//#define Qdfg

//static float q[4]={1,0,0,0};
//static float P[4][4]={{1,0,0,0},{0,1,0,0},{0,0,1,0},{0,0,0,1}};
//static float gyr[3];
//static float Rw[3][3]={{0.000001,-0.0000001,-0.0000005},{-0.0000001,0.000001,0.0000002},{-0.0000005,0.0000002,0.000002}};

void predictionStepAHRSINT32(int32_t q[4], int64_t P[4][4], int32_t gyro[3], int32_t stepSize, int64_t Rw[3][3]);

void updateStepAccAHRSINT32(int32_t q[4], int64_t P[4][4], int32_t acc[3], int64_t Ra[3][3], int32_t g0[3]);

void updateStepMagAHRSINT32(int32_t q[4], int64_t P[4][4], int32_t mag[3], int64_t Rm[3][3], int32_t m0[3], float velN, float velW);

//void predictionStepAHRSINT64(int64_t q[4], int64_t P[4][4], int64_t gyro[3], int64_t stepSize, int64_t Rw[3][3]);

//void updateStepAccAHRS(int32_t q[4], int32_t P[4][4], int32_t acc[3], int32_t Ra[3][3], int32_t g0[3]);

//void Qq(int32_t q[4], int32_t Q[12][3]);

//void dQqdq(int32_t q[4], int32_t Q[12][3]);

float getMagCorrection();

void GPSCorrection(int32_t mag1[3],float velN,float velW);

void generateq1(int32_t q1[4],int32_t q[4]);

void rotate_magINT32(int32_t mag1[3],int32_t Q1[12][3],int32_t mag[3]);

void calc_Q1_magINT32(int32_t Q1[12][3],int32_t q[4],int32_t q1[4]);

void QqINT32(int32_t Q[12][3], int32_t q[4]);

void dQqdqINT32(int32_t Q[12][3], int32_t q[4]);

void SomegaINT32(int32_t S[4][4],int32_t gyro[3], int32_t T);

//void SomegaINT64(int64_t S[4][4], int64_t gyro[3], int64_t T);

//void Sqfunc(int32_t Sq[4][3],int32_t q[4]);

void SqfuncINT32(int32_t Sq[4][3],int32_t q[4], int32_t T);

//void normalizeQ(int32_t q[4]);

void normalizeQINT32(int32_t q[4]);

void limitP(int64_t* P,int64_t* P0);

void resetP(int64_t* P, int64_t* P0);

void resetq(int32_t* q, int32_t* q0);


float quaternion2Yaw(int32_t* q);
//uint32_t SquareRoot(uint32_t a_nInput);

//void inv3x3(int32_t mat[3][3], int32_t inv[3][3]);

//#endif




