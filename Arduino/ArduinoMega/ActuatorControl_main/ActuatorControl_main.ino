#include <stdint.h>
#include <Servo.h>
#include "MessageHandler.h"
/*
================================================
This is the code that runs on the actuator controller
which controls the steering actuator and throttle servo.
A PID controller is used to regulate the steering
actuator to its reference.

The code uses several modes which dictates what is used as
reference. These are controlled by messages on a serial
bus, interpreted by the "MessageHandler" code. As a safety
feature the mode must be constantly updated otherwise it
will revert to the default mode = 0.

mode = 0 is manual mode which is the default.
In this mode the steering actuator reference is read from the
handle bar (steering wheel) with a hall sensor. In this
mode the throttle servo is set to zero, since the driver is 
pulling the wire manually.

mode = 1 is the autonomous mode in which the reference for
steering actuator and throttle servo is read from RR
arduino due.  

mode = 2 is the remote control mode. This is mostly the same
as mode = 1 but reference stems from an xbox controller.

mode = 3 is an alternative autonomous mode that was used during
testing. It is currently unused.

================================================
 */
 
// Hall sensor min and max readings
int steeringWheelMax=839; //Right turn
int steeringWheelMid=603;
int steeringWheelMin=366; //Left turn

// Actuator min and max readings 
int actuatorMax=981;//927;//820;
int actuatorMid=552;
int actuatorMin=123;//177;//284;


// These are the pins used to control the H-bridge
int pin_A =7; //Blue cable
int pin_B =4; //Orange cable
int pin_pwm=5; //Green cable

// Here we declare the filter coeffiecients of the PID.
float b0;
float b1;
float b2;
float a0;
float a1;
float a2;
// Sampling time of the PID.
float h = 0.01;

//Tuning parameters of the PID
float Kp = 0.34;  //Proportional part
float Ki = 0.0;   //Integral part. No anti-windup function implemented!
float Kd = 0.13;  //Derivative part
float Tf = 0.26;  //Time constant of derivative low-pass filter (to reduce the sensitivity to high frequency noise). Cutoff frequency (gain is below -3 dB) is 1/Tf (rad/s).

// To avoid the risk of running too fast into the mechanical limits of
// of the steering setup, virtual springs were added to the end points.

//End stop spring constants
float   Kspring = 0.68;
float     softStopMin;
float     softStopMax;
float   yspring = 0.0;

float steeringRef=0.0;
float actuatorMeas=0.0;

//Array of floats to store current and past PID outputs (control voltages).
float y[3] = {0,0,0};
//Array of floats to store current and past PID inputs (errors).
float u[3] = {0,0,0};
//mode = 0 signifies manual control (using the steering wheel).
int8_t mode = 0;
//An unsigned 32-bit variable used to keep track of time to determine the correct delay time to ensure operation at 100 Hz.
uint32_t timeMicro;

MessageHandler mh(&Serial1); // This dictates what serial port is used.
// For testing it might sometimes be easier to change this to "&Serial"
// which is the serial port connected to USB. The messages can then be
// sent directly from a computer, without going through the radio link.


// Servo used to pull the throttle wire
Servo myservo;

//Function declarations, the functions themselves are defined at the bottom.

//A function that calculates the filter coefficients of the PID, given the tuning parameters (and sample time). These are
//declared globally and does therefore not need to be given as arguments.
void calcPIDCoeff();

//A function that takes measurements and calculates the control error.
void updateU();

//A function that calculates the output of the PID based on the control error.
void updatePID();

// A function that checks if the end stops are close and if the spring forces 
// should be applied.
void checkEndStop();

//This function takes the calculated control output and applies the H-bridge using pin_A, pin_B and pin_pwm.
//Having this as a separate function makes it easy to comment it out while testing.
void updateOutput(float output);

// Function used to convert the value read from the hall
// sensor to cm.
float steeringWheel2cm(int steeringWheelMeas);

// Function used to convert the value read from the actuator
// to cm.
float actuator2cm(int actuatorMeas);


//Run once at the beginning of the program.
void setup() {
  calcPIDCoeff(); //Calculate PID coefficients.
  Serial.begin(115200); //Begin the serial communication at baud rate of 115200 (bits/sec).
                        //The serial monitor in the arduino IDE must be set to use the same
                        // rate, otherwise it will print random nonsense.
                        // Comment out this line if line 89 is changed to "&Serial".
  
  Serial1.begin(19200); //Begin the serial communication with the RR arduino due.
                        //Change this to "Serial.begin(19200);" if line 89 is changed to "&Serial".
                        
  myservo.attach(9); // The control wire for the throttle servo is connected to pin 9.
  myservo.write(14); // 14 degrees corresponds to zero throttle.


// Virtual spring starts.
  softStopMin = actuator2cm(actuatorMin+54);
  softStopMax = actuator2cm(actuatorMax-54);

  // Give the electronics some time to settle before starting.
  delay(500);
}
//
void loop() {
  timeMicro = micros(); //Save the time at the start of the loop (micros() returns the time in microseconds since last startup).
  mh.checkMessages(400); //Spend 400 us to check for new messages from the arduino due.
  
  
  Serial.print("Mode from radio: ");
  Serial.println(mh.getMode());
  updateU(); //Update the control error.
  updatePID();//Update PID output.
  Serial.print("PID output: ");
  Serial.println(y[0]); //Uncomment to see the latest PID output in the serial monitor.
  checkEndStop(); //Calculates the spring force yspring.
  Serial.print("Control output: ");
  Serial.println(y[0]+yspring);
  updateOutput(y[0]+yspring);//Apply PID output and spring force.
  
  if(mode!=0)
  {
    // Activate throttle servo if not in manual mode.
    Serial.println("Throttle servo activated!");
    myservo.write(map(mh.getThrottleRef(),0,100,14,90)); //14 degrees is zero throttle, 90 degrees is full throttle.
  }
  else
  {
    // Manual mode, throttle servo set to zero throttle.
    myservo.write(map(0,0,100,14,90));
  }
  
  delayMicroseconds(10000-(micros()-timeMicro)); //Calculate the time nessesary to ensure 100 Hz operation. This will work even if the
  //timer in micros() overflows (after 4294967295 it will restart at 0).

}

void calcPIDCoeff(){
  //Result of the discretization (Tustin).
  a0 = 4*Tf+2*h;
  b0 = ((2*Tf+h)*(2*Kp+h*Ki)+4*Kd)/a0;
  b1 = (-8*Kd-2*Kp*(2*Tf+h)+h*Ki*(2*Tf+h)-2*Tf*(2*Kp+h*Ki)+h*(2*Kp+h*Ki))/a0;
  b2 = (4*Tf*Kp-2*h*Tf*Ki-2*h*Kp+h*h*Ki+4*Kd)/a0;
  a1 = (-8*Tf)/a0;
  a2 = (4*Tf-2*h)/a0; 
}


void updateU(){
  //Increment the time index by shifting the values in u[] one step.
  u[2]=u[1];
  u[1]=u[0];


 mode=mh.getMode(); //Get mode from arduino due. Defaults to mode = 0 if connection is lost.

  if(mode==0) //This is if the steering wheel is in control
  {
    //Both are transformed to cm and error is calculated
    

    steeringRef = steeringWheel2cm(analogRead(A2));

    actuatorMeas = actuator2cm(analogRead(A0));

    Serial.print("Steeringwheel: ");
    Serial.println(analogRead(A2));
    Serial.print("Actuator: ");
    Serial.println(analogRead(A0));
    Serial.println("");

//    Serial.print("Steeringwheel: ");
//    Serial.println(steeringRef);
//    Serial.print("Actuator: ");
//    Serial.println(actuatorMeas);
//    Serial.println("");

    u[0] = steeringRef-actuatorMeas;
    Serial.print("Control error: ");
    Serial.println(u[0]);

    //u[0]=0;
    
  }
  else if(mode==1||mode==3)
  { 
    // Autonomous mode
    // mh.getSteeringAngleRef() comes from the arduino due and lies between +/- 100.
    float steeringRef = ((float)mh.getSteeringAngleRef())*0.035;  // *0.035 to convert +/- 100 to +/- 3.5 cm for full range of 7.0 cm.
    
    float actuatorMeas = actuator2cm(analogRead(A0));

    Serial.print("SteeringAutonomous: ");
    Serial.println(steeringRef);
    Serial.print("Actuator: ");
    Serial.println(actuatorMeas);
    Serial.println("");

    u[0] = steeringRef-actuatorMeas;  // Control error
    Serial.print("Control error: ");
    Serial.println(u[0]);
  }
  else if(mode==2)
  {
    // Remote control mode 
    // mh.getSteeringAngleRef() comes from the arduino due and lies between +/- 100.
    float steeringRef = ((float)mh.getSteeringAngleRef())*0.035;  // *0.035 to convert +/- 100 to +/- 3.5 cm for full range of 7.0 cm.
    
    float actuatorMeas = actuator2cm(analogRead(A0));

    Serial.print("SteeringXbox: ");
    Serial.println(steeringRef);
    Serial.print("Actuator: ");
    Serial.println(actuatorMeas);
    Serial.println("");

    u[0] = steeringRef-actuatorMeas;  // Control error
    Serial.print("Control error: ");
    Serial.println(u[0]);
  }
}

float steeringWheel2cm(int steeringWheelMeas){ //Maps steering wheel data to a reference in cm
  if(steeringWheelMeas>steeringWheelMax)
    steeringWheelMeas=steeringWheelMax;
  if(steeringWheelMeas<steeringWheelMin)
    steeringWheelMeas=steeringWheelMin;
  float temp=((float)(steeringWheelMeas-steeringWheelMid)/(steeringWheelMax-steeringWheelMin))*7.0; // Full range 7 cm.
  return temp;
}

float actuator2cm(int actuatorMeas){

  float temp=((float)(actuatorMeas-actuatorMid)/(actuatorMax-actuatorMin))*7.0; // Full range 7 cm.
  return temp;
}

void updatePID(){
  //Increment the time index by shifting the values in y[] one step.
  y[2]=y[1];
  y[1]=y[0];
  y[0] = u[0]*b0+u[1]*b1+u[2]*b2-y[1]*a1-y[2]*a2;

}

void checkEndStop(){
  if(actuatorMeas<softStopMin)
  {
    yspring=Kspring*(softStopMin-actuatorMeas);
  }
  else if(actuatorMeas>softStopMax)
  {
    yspring=Kspring*(softStopMax-actuatorMeas);
  }
  else
  {
    yspring=0.0;
  }  
}

void updateOutput(float output){
  int16_t temp = output*255; // y[0] is a number between -1 and 1 (full reverse and full forward respectively) and needs to be rescaled
  // before being applied to the pwm pin. analogWrite(PIN,x) takes an integer x between 0 and 255 as input (0 = 0 voltage, 255 = battery voltage). 
  if(temp>0)
  {
    //The direction of the motor is determined by pin_A and pin_B.
    digitalWrite(pin_A,LOW);
    digitalWrite(pin_B,HIGH);
    //Actuator out
    
  }
  else if(temp<0)
  {
    temp=-temp; //The pwm value must be a positive number.
    
    digitalWrite(pin_B,LOW);
    digitalWrite(pin_A,HIGH);
    //Actuator in
  }
  if(temp>255)
  {
    temp=255; //Limit the max pwm value.
  }

  if(temp<20)
  {
    temp=0; //If the pwm is set so low that the motor won't even turn, 0 is given instead.
  }

  analogWrite(pin_pwm,temp);
  Serial.print("PWM: ");
  Serial.println(temp);
}
