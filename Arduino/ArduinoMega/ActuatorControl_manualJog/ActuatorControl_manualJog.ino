#include <stdint.h>
#include <Servo.h>
#include "MessageHandler.h"

/*
======================================================
This code was used to manually "jog" the actuator, for example for 
calibration. It uses the steering input from MATLAB directly as PWM signal
to the H-bridge without any position feedback. The code bypasses the PID.

The serial port specified on line 60 can be changed to "&Serial" instead of
"&Serial1" to make it read from the USB bus instead of going through the
radio link.
======================================================
*/

// These are the pins used to control the H-bridge
int steeringWheelMax=799; //Right turn
int steeringWheelMid=564;
int steeringWheelMin=328; //Left turn

int actuatorMax=972;
int actuatorMid=520;
int actuatorMin=100;

float steeringWheel2cm(int steeringWheelMeas);

float actuator2cm(int actuatorMeas);


int pin_A =7; //Blue cable
int pin_B =4; //Orange cable
int pin_pwm=5; //Green cable

// Here we declare the filter coeffiecients of the PID.
float b0;
float b1;
float b2;
float a0;
float a1;
float a2;
// Sampling time of the PID.
float h = 0.01;

//Tuning parameters of the PID
float Kp = 0.34;  //Proportional part
float Ki = 0.0;   //Integral part. No anti-windup function implemented!
float Kd = 0.13;  //Derivative part
float Tf = 0.26;  //Time constant of derivative low-pass filter (to reduce the sensitivity to high frequency noise). Cutoff frequency (gain is below -3 dB) is 1/Tf (rad/s).

//Array of floats to store current and past PID outputs (control voltages).
float y[3] = {0,0,0};
//Array of floats to store current and past PID inputs (errors).
float u[3] = {0,0,0};
//mode = 0 signifies manual control (using the steering wheel).
int8_t mode = 0;
//An unsigned 32-bit variable used to keep track of time to determine the correct delay time to ensure operation at 100 Hz.
uint32_t timeMicro;

MessageHandler mh(&Serial1);

Servo myservo;

//Function declarations, the functions themselves are defined at the bottom.

//A function that calculates the filter coefficients of the PID, given the tuning parameters (and sample time). These are
//declared globally and does therefore not need to be given as arguments.
void calcPIDCoeff();

//A function that takes measurements and calculates the control error.
void updateU();

//A function that calculates the output of the PID based on the control error.
void updatePID();

//This function takes the calculated control output and applies the H-bridge using pin_A, pin_B and pin_pwm.
//Having this as a separate function makes it easy to comment it out while testing.
void updateOutput();


//Run once at the beginning of the program.
void setup() {
  calcPIDCoeff(); //Calculate PID coefficients.
  Serial.begin(19200); //Begin the serial communication at baud rate of 115200 (bits/sec). The serial monitor in the arduino IDE must be set to use the same
  // rate, otherwise it will print random nonsense.
  Serial1.begin(19200);
  myservo.attach(9);
  myservo.write(14);
  delay(500);
}
//
void loop() {
  timeMicro = micros(); //Save the time at the start of the loop (micros() returns the time in microseconds since last startup).
  mh.checkMessages(400);
  mode=mh.getMode();
  Serial.println(analogRead(A0)); //Print analog signal read from actuator
  if(mode!=0)
  {
    myservo.write(map(mh.getThrottleRef(),0,100,14,90)); 

    // This code uses the steering input from MATLAB as PWM signal to the H-bridge
    // without any position feedback. This allows the user to "jog" the actuator into
    // position. The signal is scaled by 0.01 to make it less sensitive and easier to 
    // control. 
    y[0] = mh.getSteeringAngleRef()*0.01;
    
    updateOutput();//Apply y[0].
  }
  else
  {
    myservo.write(14);
    y[0]=0.0;
    updateOutput();//Apply PID output.
  }
  
  delayMicroseconds(10000-(micros()-timeMicro)); //Calculate the time nessesary to ensure 100 Hz operation. This will work even if the
  //timer in micros() overflows (after 4294967295 it will restart at 0).

}

void calcPIDCoeff(){
  //Result of the discretization (Tustin).
  a0 = 4*Tf+2*h;
  b0 = ((2*Tf+h)*(2*Kp+h*Ki)+4*Kd)/a0;
  b1 = (-8*Kd-2*Kp*(2*Tf+h)+h*Ki*(2*Tf+h)-2*Tf*(2*Kp+h*Ki)+h*(2*Kp+h*Ki))/a0;
  b2 = (4*Tf*Kp-2*h*Tf*Ki-2*h*Kp+h*h*Ki+4*Kd)/a0;
  a1 = (-8*Tf)/a0;
  a2 = (4*Tf-2*h)/a0; 
}


void updateU(){
  //Increment the time index by shifting the values in u[] one step.
  u[2]=u[1];
  u[1]=u[0];


 mode=mh.getMode();

  if(mode==0) //This is if the steering wheel is in control
  {
    //Both are transformed to cm and error is calculated
    

    float steeringRef = steeringWheel2cm(analogRead(A2));

    float actuatorMeas = actuator2cm(analogRead(A0));

//    Serial.print("Steeringwheel: ");
//    Serial.println(analogRead(A2));
//    Serial.print("Actuator: ");
//    Serial.println(analogRead(A0));
//    Serial.println("");

    Serial.print("Steeringwheel: ");
    Serial.println(steeringRef);
    Serial.print("Actuator: ");
    Serial.println(actuatorMeas);
    Serial.println("");

    u[0] = steeringRef-actuatorMeas;
    Serial.print("Control error: ");
    Serial.println(u[0]);
    
  }
  else if(mode==1||mode==3)
  {
     float steeringRef = ((float)mh.getSteeringAngleRef())*0.0375;
    
    float actuatorMeas = actuator2cm(analogRead(A0));

    Serial.print("SteeringAutonomous: ");
    Serial.println(steeringRef);
    Serial.print("Actuator: ");
    Serial.println(actuatorMeas);
    Serial.println("");

    u[0] = steeringRef-actuatorMeas;
    Serial.print("Control error: ");
    Serial.println(u[0]);
  }
  else if(mode==2)
  {
    float steeringRef = ((float)mh.getSteeringAngleRef())*0.0375; 
    
    float actuatorMeas = actuator2cm(analogRead(A0));

    Serial.print("SteeringXbox: ");
    Serial.println(steeringRef);
    Serial.print("Actuator: ");
    Serial.println(actuatorMeas);
    Serial.println("");

    u[0] = steeringRef-actuatorMeas;
    Serial.print("Control error: ");
    Serial.println(u[0]);
  }
}

float steeringWheel2cm(int steeringWheelMeas){ //Maps steering wheel data to a reference in cm
  if(steeringWheelMeas>steeringWheelMax)
    steeringWheelMeas=steeringWheelMax;
  if(steeringWheelMeas<steeringWheelMin)
    steeringWheelMeas=steeringWheelMin;
  float temp=((float)(steeringWheelMeas-steeringWheelMid)/(steeringWheelMax-steeringWheelMin))*-8.0; //8.0 to avoid reaching actuator limits
  return temp;
}

float actuator2cm(int actuatorMeas){

  float temp=((float)(actuatorMeas-actuatorMid)/(actuatorMax-actuatorMin))*9;
  return temp;
}

void updatePID(){
  //Increment the time index by shifting the values in y[] one step.
  y[2]=y[1];
  y[1]=y[0];
  y[0] = u[0]*b0+u[1]*b1+u[2]*b2-y[1]*a1-y[2]*a2;

}

void updateOutput(){
  int16_t temp = y[0]*255; // y[0] is a number between -1 and 1 (full reverse and full forward respectively) and needs to be rescaled
  // before being applied to the pwm pin. analogWrite(PIN,x) takes an integer x between 0 and 255 as input (0 = 0 voltage, 255 = battery voltage). 
  if(temp>0)
  {
    //The direction of the motor is determined by pin_A and pin_B.
    digitalWrite(pin_A,LOW);
    digitalWrite(pin_B,HIGH);
    
  }
  else if(temp<0)
  {
    temp=-temp; //The pwm value must be a positive number.
    
    digitalWrite(pin_B,LOW);
    digitalWrite(pin_A,HIGH);
  }
  if(temp>70)
  {
    temp=70; //Limit the max pwm value.
  }

  analogWrite(pin_pwm,temp);
//  Serial.print("PWM: ");
//  Serial.println(temp);
}
