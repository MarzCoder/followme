#include <Arduino.h>
#include <HardwareSerial.h>
#include <stdint.h>



//#ifndef MessageHandler_H
//#define MessageHandler_H

class MessageHandler{
  public:
    //Constructor
    MessageHandler();        //Uses Serial2 by default

    MessageHandler(HardwareSerial* hwSerial);   //Uses the referenced serial interface

    int       checkMessages(uint16_t microSeconds); //Reads the serial buffer and processes the messages. This
                                                    //function need to be called at regular intervalls to keep
                                                    //the control signals up to date. The argument microSeconds
                                                    //specifies the time the function is allowed to run for.
                                                    
    uint8_t   getMode();                            //Returns the mode.
    int8_t    getSteeringAngleRef();                //Returns the steering angle reference.
    uint8_t   getThrottleRef();                     //Returns the throttle reference.
    void      sendSteeringAngle(uint16_t angle);      //Sends the current steering angle.

  private:
    uint8_t   mode;                   //Holds the current mode (0 by default).
    int8_t    steeringAngleRef;       //Holds the current steering angle reference (0 by default).
    uint8_t   throttleRef;            //Holds the current throttle reference (0 by default).   
    int       messagePosition;        //Used to keep track of where in the message we are.
    uint8_t   messageID;              //Holds the ID of the latest message.
    uint8_t   lastByte;               //Last byte read from the serial buffer.
//    uint8_t   temp[4];                //Not used
//    uint8_t*  tempPointer;            //Not used
    uint32_t  timeOfLastModeMessage;  //Holds the time of the last mode message. If the mode has not been updated
                                      //for a while the mode is changed back to 0.
    HardwareSerial* _HardSerial;      //Reference to the serial interface to use (Serial2 by default).
                                      //19200 baud is used.
};



//#endif
