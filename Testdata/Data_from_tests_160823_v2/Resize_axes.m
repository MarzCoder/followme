function [axisRange]=Resize_axes(x,axisRange,axisLength)
    if(x(1)>axisRange(2))
            axisRange = axisRange + [axisLength axisLength 0 0];
            axis(axisRange);
        elseif(x(1)<axisRange(1))
            axisRange = axisRange - [axisLength axisLength 0 0];
            axis(axisRange);
        elseif(x(2)>axisRange(4))
            axisRange = axisRange + [0 0 axisLength axisLength];
            axis(axisRange);
        elseif(x(2)<axisRange(3))
            axisRange = axisRange - [0 0 axisLength axisLength];
            axis(axisRange);
    end
end