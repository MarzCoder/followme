clc; 
clear all; 

%%We need to polish the noisy trajectory data. 

load('trueTrajectory.mat'); 
load('generatedSamples.mat');

x_sn = xs+x_noise; 
y_sn = ys+y_noise; 

Pos_raw = [x_sn' y_sn']; 
Pos_noiseFree   = [xs' ys']; 

%% Try single pole low pass 
Ts = GPS_T;   %Sampling time 
Tf = GPS_T*5; %Filter time constant  

% Make the filter go steady state at the initial measurement before
% starting for real. 
Noffs = 60; 
Pos_raw_offs = [Pos_raw(1,1)*ones(Noffs,1) Pos_raw(1,2)*ones(Noffs,1); Pos_raw] 

Pos_singlePole = singlePoleFilter(Pos_raw_offs,Tf, Ts ) ;

Pos_singlePole = Pos_singlePole(Noffs+1:end,:); 

%Pos_singlePole = singlePoleFilter(Pos_singlePole,Tf, Ts ) ; %What happens if we filter it again


%% Try double pole low pass 

fc_dp = 0.1;            %Cutoff frequency normalized to sample frequency

[b_dp,a_dp] = butter(4,fc_dp);  

Pos_doublePole = filter(b_dp,a_dp, Pos_raw_offs );  

Pos_doublePole = Pos_doublePole(Noffs+1:end, :); 

%% Gauss kernel smoother
sigma = 1; %Gauss window parameter in seconds
Pos_gauss = gaussKernelSmoother(t, Pos_raw, sigma); 


%% Moving average
windowSize      = 30;
b =     (1/windowSize)*ones(1,windowSize);
a =      1;

Pos_MOV = filter(b,a,Pos_raw_offs);

%Remove the initial transient of the filter 
Pos_MOV = Pos_MOV(Noffs+1:end, :); 

%% Kalman using CA model
N = length(x_sn); 

X_hat_Px = [x_sn(1), 0 , 0]'; 
X_hat_Py = [y_sn(1), 0 , 0]';  

P_hat_x = zeros(3,3); 
P_hat_y = zeros(3,3);

R0 = var_xy;
Q0 = 0.5; 

Pos_kalman_CA       = zeros(N,2); 
Pos_kalman_CA (1,:) = [x_sn(1), y_sn(1)]; 

for i=2:length(x_sn)
    
    [X_hat_Px, P_hat_x] = kalman_CA(X_hat_Px, P_hat_x, x_sn(i), GPS_T, R0, Q0); 
    [X_hat_Py, P_hat_y] = kalman_CA(X_hat_Py, P_hat_y, y_sn(i), GPS_T, R0, Q0);
    
    %Save position values 
   Pos_kalman_CA(i,:)  =[X_hat_Px(1) X_hat_Py(1) ]; 
end

%% Get MMSE 
MSE_SP      = calculateMSE( Pos_noiseFree, Pos_singlePole)
MSE_DP      = calculateMSE( Pos_noiseFree, Pos_doublePole)
MSE_Kalman  = calculateMSE( Pos_noiseFree, Pos_kalman_CA)
MSE_Raw     = calculateMSE( Pos_noiseFree, Pos_raw)
MSE_gauss   = calculateMSE( Pos_noiseFree, Pos_gauss)

%% Minimum distance filter 
d_min = 7; 

Pos_distFiltered    = distanceFilter(Pos_raw, d_min); 

Pos_MOV_dist        = distanceFilter(Pos_MOV, d_min); 
Pos_singlePole_dist = distanceFilter(Pos_singlePole, d_min); 
Pos_doublePole_dist = distanceFilter(Pos_doublePole, d_min); 
Pos_kalman_CA_dist  = distanceFilter(Pos_kalman_CA, d_min);
Pos_gauss_dist      = distanceFilter(Pos_gauss,      d_min); 

%% Plots 
close all; 

figure; 

plot(x_true, y_true,'b--','LineWidth',3); 
hold on;
plot(Pos_distFiltered(:,1), Pos_distFiltered(:,2),'g-+'); 
%plot(x_sn, y_sn,'k-+'); 
%plot(Pos_distFiltered(:,1), Pos_distFiltered(:,2), '-ro','LineWidth',3); 
plot(Pos_MOV_dist(:,1), Pos_MOV_dist(:,2), '-kx','LineWidth',2); 
%plot(Pos_singlePole_dist(:,1), Pos_singlePole_dist(:,2),'-ro');%,'LineWidth',3); 
plot(Pos_doublePole_dist(:,1), Pos_doublePole_dist(:,2),'--bo');
plot(Pos_gauss_dist(:,1), Pos_gauss_dist(:,2), 'r-o','LineWidth',2); 

ylabel(' y-position [meters]'); 
xlabel(' x-position [meters]'); 

legend('True',...
        'Just distance filter',['Mov, N=', num2str(windowSize)],...
         ['Double pole, Fc =',num2str(fc_dp)],...
         ['Gauss \sigma=', num2str(sigma)]   ); 
     
      %['Single pole, Tf =',num2str(Tf),' s'],...

title(['Noise std. dev ', num2str(sqrt(var_xy)),'m',' , distance filtered to ', num2str(d_min),' m'] );

figure; 
plot(x_true, y_true,'b-','LineWidth',3); 
hold on;
plot(Pos_singlePole_dist(:,1), Pos_singlePole_dist(:,2),'-ro');
plot(Pos_doublePole_dist(:,1), Pos_doublePole_dist(:,2),'--bo');
plot(Pos_kalman_CA_dist(:,1),Pos_kalman_CA_dist(:,2),'-bx'); 
plot(Pos_raw(:,1), Pos_raw(:,2),'ko'); 

%Legend 
legend( ['True path'],...
        ['Single pole, MSE=', num2str(MSE_SP,2) ],...
        ['Doblee pole, MSE=', num2str(MSE_DP,2) ],...
        ['Kalman CA, MSE=', num2str(MSE_Kalman,2)],...
        ['Raw, MSE=', num2str(MSE_Raw,2)]  );
        
    
ylabel('y-position [m]');
xlabel('x-position [m]');

%Check  whats up in the time domain 
figure;

t = (0:length(xs)-1)*GPS_T; 

plot(t, xs,'b','LineWidth',2); 
hold on; 
plot(t, Pos_kalman_CA(:,1),'r'); 

plot(t, Pos_doublePole(:,1),'k--'); 
plot(t, Pos_MOV(:,1),'-ro'); 
plot(t, Pos_gauss(:,1),'r--', 'LineWidth',2)

legend( 'True position',...
        'Kalman using CA',...
         'Buttherworth double pole',...
         ['Mov using N = ' num2str(windowSize)],...
          ['Gaussian Kernel, \sigma=', num2str(sigma) ]);
     
     
xlabel('Time [s]'); 
ylabel('x- Position [m]'); 
title('Filter comparison in the time domain'); 

%% Choose what to pass on to simulation 
smoothedWaypoints = Pos_doublePole_dist; 

save('smoothedTrajectory.mat', 'x_true','y_true', 'smoothedWaypoints', 'var_xy','velOfTime'); 

