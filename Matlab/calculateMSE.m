function [ MSE ] = calculateMSE( Pos_true, Pos_meas )
% Get positions as column vectors, calulcate mean square error 

%Squared error 
SQE = (Pos_true-Pos_meas).^2; 

%Mean of euclidean distance just 
dist = sqrt(  SQE(:,1)+SQE(:,2)   ) ; 
MSE = mean( dist); 

end

