function [ Pos_filt ] = distanceFilter( Pos_raw, d_min )
%% Removes point from gps reading that are to close to one another
% Send in (x,y) positions in column vector matrix

x_sn = Pos_raw(:,1);
y_sn = Pos_raw(:,2); 

Pos_filt = zeros(0,2); 

dist =0; 


i=1; 

while i <   length(x_sn)-1
  
    Pos_filt = [Pos_filt; [x_sn(i) y_sn(i) ] ];
    
    k=0; 
    dist =0; 
    
    %Find the next point within appropriate distance
    while(dist<d_min && (i+k+1)< length(x_sn) )
        dist = sqrt((x_sn(i+1+k)-x_sn(i))^2 +(y_sn(i+1+k)-y_sn(i))^2 ); 
        k=k+1; 
    end
    
    i=i+k; 
   
end


end

