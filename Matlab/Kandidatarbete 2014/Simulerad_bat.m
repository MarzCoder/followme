function Bat=Simulerad_bat
%Simulerad_bat Simulerar tv� Waypoints f�r b�ten �t g�ngen.
%   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.

%%% Initiering %%%
Bat=zeros(2,2);
V_Bat=zeros(1,2);

axlar=[-200 200 -200 200];
subplot(2,1,1)
axis(axlar);
grid on
button=1;
i=1;
while button ~= 3 && i<=2
    [Bat(i,1),Bat(i,2),button] = ginput(1);
    plot(Bat(i,1),Bat(i,2),'+')
    axis(axlar);
    hold on
    grid on
    
    i=i+1;
end

for k=1:length(Bat)
    del=length(Bat)/4;
    if k <= del
        V_Bat(k) = 8;
    elseif k>del && k<=2*del
        V_Bat(k) = 6;
    elseif k>2*del && k<=3*del
        V_Bat(k) = 6;
    else
        V_Bat(k) = 4;
    end
end

Bat=[Bat V_Bat'];

end

