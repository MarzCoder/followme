function [dS, Phi, bool_plats] = Banfoljning(P_skoter, P_bat, R)
        %Banfoljning Till�mpar LOS (Line of Sight).
        %   Beh�ver skoterns och b�tens position. Skickar tillbaka avst�nd mellan dem (dS), vinkel
        %   fr�n skoter till b�t (Phi) och en boolean (bool_plats) om skotern
        %   har n�tt inom radie R till WP.
        %	Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        dS = P_bat - P_skoter;
        
        if abs(dS)<=R
            bool_plats=1;
        else
            bool_plats=0;
        end
        
        Phi=atan2(dS(2),dS(1));
          
        if Phi<0
            Phi=2*pi+Phi;
        end
        
        Phi=unwrap(Phi);
        
    end
