function [Skoter] = FollowMe(Follow)
%FollowMe Huvudfunktionen som initierear Follow.
%	Tar en boolean (logical) Follow som input.
%	Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.

%%% Initiering %%%
% Starta alla moduler p� b�da b�tar (GPS, Wifi osv)
% - Uppr�tta Wifi-link
% Kontrollera att alla moduler fungerar (f�r data)
% Byt till autonomt l�ge
% Initiera styret

%%% H�mta positioner och hastighet samt f�lj efter b�t %%%
Skoter= [-100, -50, 0, 0, 0, 0];     % B�rjar tillsvidare h�r
%      Px, Py, Vf, Vd, Omega, Theta

WP_sist=Skoter([1,2]);
Ankring=[];
while Follow == 1
    %%% H�mta data fr�n moduler %%%
    %Skoter([1,2])= GPS_skoter (location)
    %Skoter(3)= GPS_skoter (hastighet fram�t)
    %Skoter(4)= accelerometer (hastighet �t sidan) 
    %Skoter(5)= gyro (vinkelhastighet)
    %Skoter(6)= kompass
    
    %Follow = Wifi_skoter;
    Follow=1;   % H�mta fr�n b�ten 
    
    if isempty(Ankring) == 0   % Om b�ten �r f�r l�ngt bort b�r den stanna 
        Bat=Ankring;           % runt en Ankrings-position.
    else
        %Bat=_Wifi_skoter      % H�mta b�tens position och hastighet
        Bat=Simulerad_bat;     % Simulerar en b�t tillsvidare
    end
    
    [Skoter, WP_sist] = Kontroll(Skoter, Bat, WP_sist);
end


end

