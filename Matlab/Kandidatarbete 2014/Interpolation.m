function [Bana, WP_sist] = Interpolation(Skoter1, Bat, WP_sist)
        %Interpolation Interpolerar fram simulerade Waypoints. 
        %   Antar f�rst ett tredjegradspolynom, s�nker polynomgraden om
        %   felet blir f�r stort.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
        %%% Begynnelsev�rden %%%
        Gradpol_max=3;              % Max Polynomgrad
        WP_s=10;                    % Avst�nd mellan simulerade WP
        Gradpol=Gradpol_max;        % Skilja p� aktuellt och max polgrad
        P_Bat=Bat(:,[1 2]);
        V_Bat=Bat(:,3);
        %WP=[Skoter1; P_Bat];       % B�rja fr�n skotern
        WP=[WP_sist([1,2]); P_Bat]; % B�rja fr�n sista WP
         
        % Antal simulerade punkter %
        Npunkter = int64( (norm(WP(2,:)- WP(1,:)) + norm(WP(3,:)- WP(2,:)))/WP_s); % J�mt f�rdelade WPs.
        
        
        %%% Interpolerar fram en bana %%%
        if Npunkter > 1     % Interpolera endast om vi har flera punkter.
            x=linspace(WP(1,1), WP(3,1),Npunkter); 
            A1=polyfit(WP(:,1), WP(:,2), 1);
            A=polyfit(WP(:,1), WP(:,2), Gradpol);
            
            P1=polyval(A1,x);
            P=polyval(A,x);
            
            SD=sum(norm(P1-P))/length(P);
            
            while SD>5 && Gradpol>1    % Minska polgrad om mycket fel.
                Gradpol=Gradpol-1;
                A=polyfit(WP(:,1), WP(:,2), Gradpol);
                P=polyval(A,x);
                SD=sum(abs(P1-P))/length(P);
            end
            
            Av=polyfit(P_Bat(:,1), V_Bat(:,1), 1);
            Banav=polyval(Av,x);
            Banax=x;
            Banay=P;

        %%% Avkommentera om man vill tvinga b�tens koord. till banan %%%
%             for j=1:length(P_Bat)
%                 for h=1:(length(Banax)-1)
%                     if P_Bat(j,1) > Banax(h) && P_Bat(j,1) < Banax(h+1)
%                         Banax(h)=P_Bat(j,1);
%                         Banay(h)=P_Bat(j,2);
%                         Banax(h+1)=[];
%                         Banay(h+1)=[];
%                         Banav(h+1)=[];
%                     end
%                 end
%             end
            
        %%% Tar bort punkter som �r f�r n�ra varandra %%%
            h=1;
            while h < length(Banax)-1
                if norm([Banax(h) - Banax(h+1), Banay(h) - Banay(h+1)]) < WP_s
                    Banax(h+1)=[];
                    Banay(h+1)=[];
                    Banav(h+1)=[];
                    h=0;
                end
                h=h+1;
            end
            
        %%% Sparar allt i en matris %%%
            Bana=[Banax' Banay' Banav'];
            Bana(1,:)=[];
            
        else    % Om antal punkter �r 1, anv�nd den direkt.
            Bana=WP;
        end     % slut p� if
        
        [rad, ~]=size(Bana);
        WP_sist=Bana(rad,:);    % Kom ih�g sista waypointen.
        
    end

