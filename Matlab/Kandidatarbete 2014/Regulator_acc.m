function a = Regulator_acc(a0, err_hast0, err_hast, dt, V)
        %Regulator_acc PID-regulator f�r jetvinkeln.
        %   Skapad av Victor B�ckman, Kandidatarbete SSYX02-15-38 v�ren 2015.
        
            % Korrigering av reg.param beroende av hastighet %
            V_skoter=sqrt(abs(V(1)^2 + V(2)^2));
            if V_skoter < 5              
                K_hast=[0.8 0.5 0.005];     % [Kp Ki Kd]
            elseif V_skoter >= 5 && V_skoter < 10
                K_hast=[0.8 0.2 0.005];
            else
                K_hast=[0.5 0.1 0.005];
            end
            
        
        int = (err_hast0 + err_hast)/2 * dt;  % Trapetsmetoden
        deriv= (err_hast - err_hast0)/dt;
        
        dV = K_hast(1)*err_hast + K_hast(2)*int + K_hast(3)*deriv;
        
        a= dV/dt;
        
    end
