close all;
clear all; 
clc; 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% While the trajectory generator generates a true trajectory, this function is supposed
%  simulate measurements from this 

load 'trueTrajectory.mat'

%% Add velocity 
%The original trajectory only has positions but no velocity
%Generate scalar velocity ds/dt here. Make it correlated to how close the
%user put the dots 

V_max = 10;             %Max velocity in m/s during track, all is normalized to this value 

delta_s = [0, diff(d_user)]; 

%So if the boat was faster, the distance difference delta_s would be
%larger. Therefore it makes sense that the largest delta_s gets the
%velocity V_max an all other proportions of V_max
V = delta_s./max(delta_s).*V_max; 

%Fit a spline to the data. Do absolute values because we dont want negative
%values 
%V_splined =  abs(spline(d_user,V,dd)); 

newNum = length(x_true); % new number of elements in the "buffed" vector

%Interpolate linearly between eacth velocity point. 
V_splined = interp1( linspace(0,1,numel(V)), V, linspace(0,1,newNum) )


%% Add noise and samle 
GPS_freq = 1;          %Pull GPS samples at this hertz 
GPS_T    = 1/GPS_freq; 

Ns = 100; %Do this many distance increments per GPS sample 

h = GPS_T/Ns; %Time increment 

%% Simulate a sampling process
dist = 0; 

i=1; %Is track index, where we are geographically 
k=0; %Is iteration intex 

%Simulate a sample sequence where we go around the track
Time = 0; 
samples = zeros(0,2); 

sampleIndexes = []; 

leaderBoat = zeros(length(x_true),4); 

%% i is our variable related to distance 
while( i< length(x_true))
    
    %% k is our distance related to time 
    k = k+1; 
   
    % At zero speed we won't increase distance, need to fix this bug
    while(V_splined(i) ==0 && i < length(V_splined)) 
        i=i+1;  
    end
    
    %Transform the velocityprofile from v(d) to v(t) 
    leaderBoat(k,:) = [k*h, V_splined(i), x_true(i), y_true(i)]; 
  
    dist = dist + h*abs(V_splined(i)); 
    
    %Update index 
   while(dist > dd(i) && i < length(dd) ) 
       i=i+1; 
   end 
   
   % Take a tample every Ns sample
   if( mod(k, Ns)==0 )
       sampleIndexes = [sampleIndexes; i]; 
   end
   
   
end 

t = (0:length(sampleIndexes)-1)*GPS_T; 

%Measurement noise 
var_xy = 16/9;   %Expected noise VARIANCE in meters 

%%% For equidistant sampling 
% N               = length(dd);
% sampleInterval  = 1;            %Draw a samples at this meter interval
%        
% ks = round(sampleInterval/delta_d); 
% 
% sampleIndexes = 1:ks:N; 
% 
% Ns = length(sampleIndexes); 
% 

xs = x_true(sampleIndexes); 
ys = y_true(sampleIndexes); 

Ns = length(sampleIndexes); 
ds = dd(sampleIndexes);

x_noise = mvnrnd(0, var_xy, Ns)'; 
y_noise = mvnrnd(0, var_xy, Ns)'; 


%% Plots 
close all; 

figure; 
 plot(x_true, y_true); 
hold on;
%h(2)=plot(xs, ys, 'ro'); 
%h(2) = plot(samples(:,1), samples(:,2), 'ro'); 
 plot(xs+x_noise, ys+y_noise,'k+'); 
 
legend( 'True trajectory','Samples'); 

figure; 
plot( (1:length(V))./length(V),V )
hold on;
plot( (1:length(V_splined))./length(V_splined), V_splined);
ylabel('Velocity [m/s]'); 
xlabel('Distance normalized in relation to path length'); 
title('Velocity profile');
legend('Crude velocity','Spline fit'); 

figure; 

plot( leaderBoat(:,1), leaderBoat(:,2) )

ylabel('Velocity [m/s]'); 
xlabel('Time'); 
title('Velocity profile of time');
 


save('generatedSamples.mat','t','xs', 'ys','ds','var_xy','x_noise','y_noise','GPS_T', 'leaderBoat');






