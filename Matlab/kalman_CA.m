function [ X_hat, P_hat ] = kalman_CA(X_hat, P_hat, y, Ts, R0, Q0)

% Used for generating C-code for a Kalman filter using a CA prediction step

%#codegen 
Nx = 3; 
 
A = [1  Ts  Ts^2/2; 
     0  1   Ts;
     0  0   1];
 
B = [0;0;1]; 
H = [1  0  0]; 

%Qk = B*Q0*B'; 
 
% persistent X_hat P_hat 
% 
% if isempty(X_hat)
%    X_hat = zeros(Nx,1); 
%    P_hat = zeros(Nx,Nx); 
% end

   %% Prediction step
   
   %% Do prediction of states X
    X_hat = A*X_hat; 
    
    %% Predict covariance P 
    P_hat = A*P_hat*A'+B*Q0*B'; 
    
    %% Update step 
  
    %% Calculate S 
    S =(H*P_hat*H'+R0);
    
    %% Calculate K
    K = P_hat*H'/S; 

    %% Kalman output for Xhat
    X_hat = X_hat + K*(y-H*X_hat); 
    
    %% Kalman outpout for Phat
    P_hat = P_hat-K*S*K';
       
end

