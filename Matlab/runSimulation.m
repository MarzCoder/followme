close all;
clear all; 

%load('trueTrajectory.mat'); 
load('smoothedTrajectory.mat'); 
load('generatedSamples.mat');

%This checkout 2016-12-15 diverges from the path

%% Parameters 

parameters; %Parameters of hull, engines etc

%Noise used in simulink model

simulatedVariance = struct( 'speed', 1,...
                            'position', var_xy,...
                            'angle', (pi/12)^2); 

PID_angle = struct('Kp',    0.3,...
                   'Ki',    0, ...
                   'Kd',    0.3,...
                   'Tf',    0); 
               
PID_speed = struct( 'Kp',   1,...
                    'Ki',   0.1, ...
                    'Kd',   0,...
                    'Tf',   0.1); 
                
 %wayPointsParams = struct ('distanceFilter',3); %The waypoints have to be this long from each other
 
 %% Waypoints
minimumDistanceWaypoints =    5;                %Minimum distance between waypoints
GPS_T                    =    0.1;                %GPS sampling interval in seconds
initPos                  =    leaderBoat(1, 3:4)';  %Where the leaderboat starts
positionTolerance        =    2; %The distance in meters in relation to a waypoint, to say we've cleared it 

%% Rescue runner 

%Start a bit away from the leaderboat, at any angle
x0 = initPos(1) -20; 
y0 = initPos(2) +5; 
theta0          = 0; 
Ts = 0.01;      

r_safe = 10; %The rescue runner o a waypoint must never be closer than to the runner than this [meters]
D_set = 30; %The rescue runner will follow a waypoint this long from the leader boat


% theta_ref = [theta0; atan2( diff(ys), diff(xs) )]
% theta_ref0 = theta_ref(2); 
% v_ref = ones(size(theta_ref))*0.1; 

%% Run simulation

%Generate setpoints with noise
%waypoints = [xs, ys, theta_ref, v_ref ];

%We stop the simulation when the leaderboat is done
T_end = leaderBoat(end,1); 

sim( 'controllerAndPlant', 2*T_end); 

%% Plots 
close all; 

x_out = squeeze(position.data(1,1,:));
y_out = squeeze(position.data(2,1,:)); 

%All waypoints that the leaderboat dropped
x_wp_a = allWaypoints.data(:,1);
y_wp_a = allWaypoints.data(:,2);

%The waypoints the rescurunner tried to pursue
x_wp_p = pursuedWaypoints.data(:,1);
y_wp_p = pursuedWaypoints.data(:,2);



plot(x_out, y_out,'k--'); 
hold on; 
plot(x_out(1), y_out(1), 'k+','MarkerSize',10);  
plot(xs(1), ys(1), 'r+','MarkerSize',10); 
plot(x_wp_a, y_wp_a, 'ro');  
plot(x_wp_p, y_wp_p, 'k+');  
plot(x_true, y_true); 

legend('Rescue Runner',...
        'Rescue Runner start point',...
        'Leader boat start point',...
        'Issued Waypoints',...
        'Pursued waypoints',...    
        'Desired trajectory' ); 
                
moment =  squeeze(controlsignals.data(1,1,:));
thrust =  squeeze(controlsignals.data(2,1,:));


% Plot with control signals 

figure;

%subplot(2,1,1); 
angle       = angleController.data(:,1); 
angleRef    = angleController.data(:,2); 
angleErr    = angleController.data(:,3); %The smallest angle in order to be  directed towards the target


%Find all points where waypoints was changed and plot
[~, IA, ~ ] = unique(x_wp_p); 
edges = ones(size(x_wp_p)); 

plot(angleController.time, angle); 
hold on; 
plot(angleController.time, angleRef, 'k--'); 
%plot(angleController.time, angleErr, 'r--'); 
stem(pursuedWaypoints.time(IA), edges(IA),'r');
%legend('Angle', 'Angle reference', 'Error to controller','Waypoint update');
legend('Angle', 'Angle reference','Waypoint update');
%Plot velocity
%subplot(2,1,2); 
figure;
velocity    = velocityController.data(:,1); 
velocityRef = velocityController.data(:,2); 

t_vel = velocityController.time; 

plot(t_vel, velocity); 
hold on; 
plot(t_vel, velocityRef, 'k--');
legend('Velocity','Velocity reference'); 


figure; 

moment = controlsignals.data(:,1); 
actualMoment = actualControlsignals.data(:,1); 

subplot(2,1,1);
plot(controlsignals.time, moment,'k');
hold on; 
plot(actualControlsignals.time, actualMoment,'r');
axis auto; 
plot(controlsignals.time, ones(size(controlsignals.time)).*maxMoment,'k--'); 
ylabel('Nm');
legend('Requested moment','Actual moment','Moment saturation','Thrust'); 
% 
% subplot(2,1,2);
% plot(controlsignals.time,thrust,'r');
% hold on 
% %plot(controlsignals.time, ones(size(controlsignals.time)).*maxThrust,'r--'); 
% legend('Moment'); 
