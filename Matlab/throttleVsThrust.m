clc; 
clear all;
close all; 

parameters; 

%Motor characteristics
p1 = 1; 
p2 = 1; 
p3 = 1; 

%Engine insignal 
gamma = 0:0.001:1; 

omega =@(KQ) [(2.*KQ.*OMEGA_0.^3+2.*gamma.*Pmax).^(-1).*(gamma.*OMEGA_0.* ...
            Pmax+gamma.^(1/2).*OMEGA_0.*Pmax.^(1/2).*(4.*KQ.*OMEGA_0.^3+5.* ...
            gamma.*Pmax).^(1/2))];
        
%Shaft power 
P =@(KQ) KQ*omega(KQ).^3; 

%Initial thrust
F0 = CF0*P(KQ); 

%% Plots 
close all; 
set(0,'defaultTextInterpreter','latex'); %trying to set the default as latez

figure; 

figure; 
plot(gamma, omega(KQ)); 

ylabel('Angular speed [rad/s]'); 
xlabel('Power/torque demand $\gamma$');

figure; 
plot(gamma, F0)
%hold on; 
%plot(gamma, ones(size(F0))*F0max,'k--'); 
ylabel('Trust $F_0$'); 
xlabel('Power/torque demand $\gamma$');
legend(['Thrust, $K_Q=$', num2str(KQ,3),' $C_{F0}$=',num2str(CF0,2) ])
%set(gca,'Ytick',[]);


%% Power curves
figure; 

h(1) = plot(gamma, ones(size(gamma))*Pmax, 'k--', 'LineWidth',3)
hold on; 

KQi = [0.00005, 0.0002, 0.0005, 0.01, 0.1]; 

h(2) =plot(gamma, P( KQi(1)),'--k')
hold on; 
h(3) = plot(gamma, P( KQi(2)),'-r','LineWidth',3)
h(4) = plot(gamma, P( KQi(3)),':b','LineWidth',2)
h(5) = plot(gamma, P( KQi(4)),'-.m', 'LineWidth',1)

l = legend(h,'Max engine power',...
            ['$K_Q = $ ', num2str(KQi(1),'%.0e')],... 
            ['$K_Q = $',  num2str(KQi(2),'%.0e')],... 
            ['$K_Q = $',  num2str(KQi(3),'%.0e')],... 
            ['$K_Q = $',  num2str(KQi(4),'%.0e')])

set(l,'Interpreter','latex')

ylabel('Shaft power P [w]'); 
xlabel('Power/torque demand $\gamma$');
%set(gca,'Ytick',[]);







