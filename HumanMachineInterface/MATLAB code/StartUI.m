
close all; 

Fread = 10; %How often we should read the LB 

%Clear all previous connection to avoid conflicts
clearAllSerialPorts; 

%Check some where else to which COM port the arduino is connected 
Arduino = serial('COM15','BaudRate',115200);

%Should always be contious, else there can be a loss of data
Arduino.ReadAsyncMode = 'continuous';

%Use an 1 mb buffer 
Arduino.InputBufferSize = 1000000; 

%Connect_to_arduino

fopen(Arduino);

flushinput(Arduino);  
fread(Arduino,1,'int8')
panel(Arduino, Fread); % The joystick control has been commented out. If a joystick
% is used to remotely control the RR this must be uncommented.

