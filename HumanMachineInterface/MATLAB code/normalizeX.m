function [normX] = normalizeX(x)
normX=x/norm(x);
end