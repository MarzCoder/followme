%Read throttle
function throttle_ref = Read_throttle(joyObject)
    [axes, buttons, ~] = read(joyObject);
%     if(axes(3)>0.95)
%         axes(3)=1;
%     end
% 
%     throttle_ref = -(axes(3)-1)/2*100;
    axes(2)=-axes(2);
    if(axes(2)<0)
        axes(2)=0;
    end
    if(axes(2)>1)
        axes(2)=1;
    end
    throttle_ref=axes(2)*50;
end
