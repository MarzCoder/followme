%Read steering
function steering_ref = Read_steering(joyObject)

[axes, buttons, ~] = read(joyObject);
if(axes(3)<-1)
    axes(3)=-1;
end
if(axes(3)>1)
    axes(3)=1;
end
% steering_ref=100*axes(1);
steering_ref=-100*axes(3);
end