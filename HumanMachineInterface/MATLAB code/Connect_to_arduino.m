
serialInfo = instrhwinfo('serial'); %This reads information about connected
%hardware. An Arduino will typically show up as 'COMX' (where X is the port
%number) in the data field 'AvailableSerialPorts'.

%This checks if there already exists a serial object named "Arduino".
%If not, it creates a serial object named "Arduino" at the port found
%above, with the baud rate set to 115200.
if(~exist('Arduino','var')) 
%         Arduino = serial(serialInfo.AvailableSerialPorts{1},'BaudRate',115200);
        Arduino = serial(serialInfo.AvailableSerialPorts{1},'BaudRate',115200);
%         Arduino = serial('COM5','BaudRate',115200);
end
%The serial object Arduino must be opened before it can be used. While it
%is open in MATLAB it cannot be used by any other program (eg. Arduino IDE)
%simultaneously. To close the port type "fclose(Arduino)"
if(~strcmp(Arduino.status,'open'))
    fopen(Arduino);
end
