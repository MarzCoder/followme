%Open arduino
if(~exist('Due'))
    serialInfo = instrhwinfo('serial');
    Due = serial(serialInfo.AvailableSerialPorts,'BaudRate',115200);
end
if(~strcmp(Due.status,'open'))
    fopen(Due);
end
flushinput(Due);